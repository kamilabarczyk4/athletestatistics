echo -e "\n### Deactivate virtual environment ###"
.venv/Scripts/deactivate.bat

echo -e "\n### Remove .venv, build and dist ###"
rm -rf .venv build dist athlete_statistics.egg-info