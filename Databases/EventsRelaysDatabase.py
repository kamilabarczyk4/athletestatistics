import mysql.connector
from Databases.settings import mysql_settings
from Entities.Relay import Relay
from Entities.Event import Event
from typing import Iterable, Optional


class EventsRelaysDatabase():
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.__events_relays_table_name = "events_relays"
        self.__event_relay_columns = (
            "id",
            "event_id",
            "relay_id"
        )

    def connection(self):
        return mysql.connector.connect(
            user=mysql_settings["user"],
            password=mysql_settings["password"],
            host=mysql_settings["host"],
            database=mysql_settings["database"]
        )

    def create_structure(self) -> bool:
        connection = self.connection()
        cursor = connection.cursor()

        command = """CREATE TABLE IF NOT EXISTS {} (
            id VARCHAR(40) PRIMARY KEY,
            event_id VARCHAR(40),
            relay_id VARCHAR(40)
            )""".format(self.events_relays_table_name)

        cursor.execute(command)

        cursor.close()
        connection.close()

    @property
    def logger(self):
        return self.__logger

    @property
    def events_relays_table_name(self):
        return self.__events_relays_table_name
    
    @property
    def event_relay_columns(self):
        return self.__event_relay_columns


    def exists(self, event_id, relay_id) -> bool:
        command = """
            SELECT * FROM {events_relays_table_name} WHERE event_id="{event_id}" AND relay_id="{relay_id}"
        """.format(
            events_relays_table_name=self.events_relays_table_name,
            event_id = event_id,
            relay_id = relay_id
        )
        connection = self.connection()
        cursor = connection.cursor()

        cursor.execute(command)
        result = cursor.fetchone()

        cursor.close()
        connection.close()

        if result is not None and result[0]:
            #self.logger.debug("Event-Relay relation {} exists in database".format(event_relay))
            return result[0]
        else:
            return False
        

    def create_event_relay_relation(self, event_relay_id: str, event: Event, relay: Relay) -> Optional[bool]:
        if self.exists(event.id, relay.id):
            return None
        
        command = """
            INSERT INTO {events_relays_table_name} ({event_relay_columns})
            VALUES
            (
                '{id}',
                '{event_id}',
                '{relay_id}'
            )
        """.format(
            events_relays_table_name=self.events_relays_table_name,
            event_relay_columns=", ".join(self.event_relay_columns),
            id=event_relay_id,
            event_id=event.id,
            relay_id=relay.id
        )

        connection = self.connection()
        cursor = connection.cursor()

        cursor.execute(command)
        connection.commit()
        cursor.close()
        connection.close()

        return True

    def readEventRelayRelation(self, event_relay_id: str):
        command = """
            SELECT {event_relay_columns} FROM {events_relays_table_name} WHERE id='{event_relay_id}' LIMIT 1
        """.format(
            event_relay_columns=", ".join(self.event_relay_columns),
            events_relays_table_name=self.events_relays_table_name,
            event_relay_id=event_relay_id
        )

        connection = self.connection()
        cursor = connection.cursor()

        cursor.execute(command)
        event_relay_row = cursor.fetchone()

        cursor.close()
        connection.close()

        if not event_relay_row:
            #self.logger.warn("Not found Event-Relay relation {} in database".format(event_relay_id))
            return None

        event = Event.from_dict({
            "id": event_relay_row[1]
        })

        relay = Relay.from_dict({
            "id": event_relay_row[2]
        })

        return [event, relay]

    def updateEventRelayRelation(self, relay: Relay, event: Event, relation_id: str ):
        connection = self.connection()
        cursor = connection.cursor()

        command = """
            UPDATE {events_relays_table_name}
            SET event_id ='{event_id}', relay_id='{relay_id}'
            WHERE id='{relation_id}'
        """.format(
            results_table_name=self.results_table_name,
            event_id=event.id,
            relay_id=relay.id,
            relation_id=relation_id
        )

        cursor.execute(command)

        connection.commit()
        cursor.close()
        connection.close()

db = EventsRelaysDatabase()
db.create_structure()


