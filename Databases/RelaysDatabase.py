import logging
import mysql.connector
from Databases.settings import mysql_settings
from Entities.Relay import Relay
from typing import Iterable, Optional


class RelaysDatabase():
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.__relays_table_name = "relays"

        self.__logger = logging.getLogger("mysqlRelaysDatabase")
        self.__relay_columns = (
            "id",
            "relay_type",
        )

    def connection(self):
        return mysql.connector.connect(
            user=mysql_settings["user"],
            password=mysql_settings["password"],
            host=mysql_settings["host"],
            database=mysql_settings["database"]
        )
    
    @property
    def logger(self):
        return self.__logger

    @property
    def relays_table_name(self):
        return self.__relays_table_name
    
    @property
    def relay_columns(self):
        return self.__relay_columns

    def create_structure(self) -> bool:
        connection = self.connection()
        cursor = connection.cursor()

        command = """CREATE TABLE IF NOT EXISTS {} (
            id VARCHAR(40) PRIMARY KEY,
            relay_type VARCHAR(255)
        )""".format(self.relays_table_name)
        cursor.execute(command)

        cursor.close()
        connection.close()


    def exists(self, relay: Relay) -> bool:
        command = """
            SELECT EXISTS(SELECT * FROM {relays_table_name} WHERE id="{relay_id}" LIMIT 1)
        """.format(
            relays_table_name=self.relays_table_name,
            relay_id=relay.id
        )
        connection = self.connection()
        cursor = connection.cursor()

        cursor.execute(command)
        result = cursor.fetchone()

        cursor.close()
        connection.close()

        if result[0]:
            #self.logger.debug("Relay {} exists in database".format(relay.id))
            return True
        else:
            return False
        

    def create_relay(self, relay: Relay) -> Optional[bool]:
        if self.exists(relay):
            return None

        command = """
            INSERT INTO {relays_table_name} ({relay_columns})
            VALUES
            (
                '{id}',
                '{relay_type}'
            )
        """.format(
            relays_table_name=self.relays_table_name,
            relay_columns=", ".join(self.relay_columns),
            id=relay.id,
            relay_type=relay.relay_type
        )

        connection = self.connection()
        cursor = connection.cursor()

        cursor.execute(command)
        connection.commit()
        cursor.close()
        connection.close()

        return True

    def readRelay(self, relay_id: str) -> Optional[Relay]:
        command = """
            SELECT {relay_columns} FROM {relays_table_name} WHERE id='{relay_id}' LIMIT 1
        """.format(
            relay_columns=", ".join(self.relay_columns),
            relays_table_name=self.relays_table_name,
            relay_id=relay_id
        )

        connection = self.connection()
        cursor = connection.cursor()

        cursor.execute(command)
        relay_row = cursor.fetchone()

        cursor.close()
        connection.close()

        if not relay_row:
            #self.logger.warn("Not found relay {} in database".format(relay_id))
            return None

        relay = Relay.from_dict({
            "id": relay_row[0],
            "relay_type": relay_row[1]

        })

        print("sql relay raw", relay)
        return relay
    
    def readAllRelays(self):
        command = """
        SELECT {relay_columns} FROM {relays_table_name}
    """.format(
        relay_columns=", ".join(self.relay_columns),
        relays_table_name=self.relays_table_name,
    )

        connection = self.connection()
        cursor = connection.cursor()

        cursor.execute(command)
        relay_rows = cursor.fetchall()

        cursor.close()
        connection.close()

        relays = []
        for relay in relay_rows:
            relays.append(Relay.from_dict({
                "id": relay[0],
                "relay_type": relay[1]
            }))
        print("sql relay raw", *relays)
        return relays
    
    def updateRelay(self, relay: Relay):
        connection = self.connection()
        cursor = connection.cursor()

        command = """
            UPDATE {relays_table_name}
            SET relay_type = '{relay_type}'
            WHRE id = '{relay_id}'
        """.format(
            relays_table_name=self.relays_table_name,
            relay_type=relay.relay_type,
            relay_id=relay.id
        )

        cursor.execute(command)

        connection.commit()
        cursor.close()
        connection.close()