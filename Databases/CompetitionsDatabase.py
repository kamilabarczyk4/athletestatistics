import mysql.connector
from Databases.settings import mysql_settings
from Entities.Competition import Competition
from typing import Iterable, Optional


class CompetitionsDatabase():
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.__competitions_table_name = "competitions"
        self.__competition_columns = (
            "id",
            "competition_name",
            "is_poly_competition"
        )

    def connection(self):
        return mysql.connector.connect(
            user=mysql_settings["user"],
            password=mysql_settings["password"],
            host=mysql_settings["host"],
            database=mysql_settings["database"]
        )

    def create_structure(self) -> bool:
        connection = self.connection()
        cursor = connection.cursor()

        command = """CREATE TABLE IF NOT EXISTS {} (
            id VARCHAR(40) PRIMARY KEY,
            competition_name VARCHAR(100),
            is_poly_competition BOOLEAN
        )""".format(self.competitions_table_name)
        cursor.execute(command)

        cursor.close()
        connection.close()

    @property
    def logger(self):
        return self.__logger

    @property
    def competitions_table_name(self):
        return self.__competitions_table_name
    
    @property
    def competition_columns(self):
        return self.__competition_columns


    def exists(self, competition: Competition) -> bool:
        command = """
            SELECT EXISTS(SELECT * FROM {competitions_table_name} WHERE id="{competition_id}" LIMIT 1)
        """.format(
            competitions_table_name=self.competitions_table_name,
            competition_id=competition.id
        )
        connection = self.connection()
        cursor = connection.cursor()

        cursor.execute(command)
        result = cursor.fetchone()

        cursor.close()
        connection.close()

        if result[0]:
            #self.logger.debug("Competition {} exists in database".format(competition.id))
            return True
        else:
            return False
        

    def create_competition(self, competition: Competition) -> Optional[bool]:
        if self.exists(competition):
            return None

        command = """
            INSERT INTO {competitions_table_name} ({competition_columns})
            VALUES
            (
                '{id}',
                '{competition_name}',
                '{is_poly_competition}'
            )
        """.format(
            competitions_table_name=self.competitions_table_name,
            competition_columns=", ".join(self.competition_columns),
            id=competition.id,
            competition_name=competition.competition_name,
            is_poly_competition=competition.is_poly_competition
        )

        connection = self.connection()
        cursor = connection.cursor()

        cursor.execute(command)
        connection.commit()
        cursor.close()
        connection.close()

        return True

    def readCompetition(self, competition_id: str) -> Optional[Competition]:
        command = """
            SELECT {competition_columns} FROM {competitions_table_name} WHERE id='{competition_id}' LIMIT 1
        """.format(
            competition_columns=", ".join(self.competition_columns),
            competitions_table_name=self.competitions_table_name,
            competition_id=competition_id
        )

        connection = self.connection()
        cursor = connection.cursor()

        cursor.execute(command)
        competition_row = cursor.fetchone()

        cursor.close()
        connection.close()

        if not competition_row:
            #self.logger.warn("Not found competition {} in database".format(competition_id))
            return None

        competition = Competition.from_dict({
            "id": competition_row[0],
            "competition_name": competition_row[1],
            "is_poly_competition": competition_row[2]

        })

        
        return competition

    def readAllCompetitions(self) -> Optional[Competition]:
        command = """
            SELECT {competition_columns} FROM {competitions_table_name}
        """.format(
            competition_columns=", ".join(self.competition_columns),
            competitions_table_name=self.competitions_table_name,
        )

        connection = self.connection()
        cursor = connection.cursor()

        cursor.execute(command)
        competition_rows = cursor.fetchall()

        cursor.close()
        connection.close()

        if not competition_rows:
            #self.logger.warn("Not found competitions in database")
            return None
        competitions = []
        for competition in competition_rows:
            competitions.append(Competition.from_dict({
                "id": competition[0],
                "competition_name": competition[1],
                "is_poly_competition": competition[2]
            }))

        return competitions

    def updateCompetition(self, competition: Competition):
        connection = self.connection()
        cursor = connection.cursor()

        command = """
            UPDATE {competitions_table_name}
            SET competition_name = '{competition_name}', is_poly_competition = '{is_poly_competition}'
            WHERE id = '{competition_id}'
        """.format(
            competitions_table_name=self.competitions_table_name,
            competition_name=competition.competition_name,
            is_poly_competition=competition.is_poly_competition,
            competition_id=competition.id
        )

        cursor.execute(command)

        connection.commit()
        cursor.close()
        connection.close()

    def removeCompetition(self, competition_id):
        connection = self.connection()
        cursor = connection.cursor()

        command = """
            DELETE FROM {competitions_table_name}
            WHERE id = '{competition_id}'
        """.format(
            competitions_table_name=self.competitions_table_name,
            competition_id=competition_id
        )
        
        cursor.execute(command)

        connection.commit()
        cursor.close()
        connection.close()