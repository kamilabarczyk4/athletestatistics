import mysql.connector
from Databases.settings import mysql_settings
from Entities.Competitor import Competitor
from Entities.Result import Result
from typing import Iterable, Optional


class ResultsDatabase():
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.__results_table_name = "results"
        self.__result_columns = (
            "id",
            "competitor_id",
            "event_competition_id",
            "result"
        )

    def connection(self):
        return mysql.connector.connect(
            user=mysql_settings["user"],
            password=mysql_settings["password"],
            host=mysql_settings["host"],
            database=mysql_settings["database"]
        )

    def create_structure(self) -> bool:
        connection = self.connection()
        cursor = connection.cursor()

        command = """CREATE TABLE IF NOT EXISTS {} (
            id VARCHAR(40) PRIMARY KEY,
            competitor_id VARCHAR(40),
            event_competition_id VARCHAR(40),
            result FLOAT
            )""".format(self.results_table_name)

        cursor.execute(command)

        cursor.close()
        connection.close()

    # @property
    # def logger(self):
    #     return self.__logger

    @property
    def results_table_name(self):
        return self.__results_table_name
    
    @property
    def result_columns(self):
        return self.__result_columns


    def exists(self, result_id: str) -> bool:
        command = """
            SELECT EXISTS(SELECT * FROM {results_table_name} WHERE id="{result_id}" LIMIT 1)
        """.format(
            results_table_name=self.results_table_name,
            result_id=result_id
        )
        connection = self.connection()
        cursor = connection.cursor()

        cursor.execute(command)
        result = cursor.fetchone()

        cursor.close()
        connection.close()

        if result[0]:
            #self.logger.debug("Resultn {} exists in database".format(result))
            return True
        else:
            return False
        

    def create_result(self, result: Result, competitor: Competitor) -> Optional[bool]:
        if self.exists(result.id):
            return None
        
        command = """
            INSERT INTO {results_table_name} ({result_columns})
            VALUES
            (
                '{id}',
                '{competitor_id}',
                '{event_competition_id}',
                '{result}'
            )
        """.format(
            results_table_name=self.results_table_name,
            result_columns=", ".join(self.result_columns),
            id=result.id,
            competitor_id=competitor.id,
            event_competition_id=result.event_competition_id,
            result=result.result
        )

        connection = self.connection()
        cursor = connection.cursor()

        cursor.execute(command)
        connection.commit()
        cursor.close()
        connection.close()

        return True

    def readResult(self, result: Result):
        command = """
            SELECT {result_columns} FROM {results_table_name} WHERE id='{result_id}' LIMIT 1
        """.format(
            result_columns=", ".join(self.result_columns),
            results_table_name=self.results_table_name,
            result_id=result.id
        )

        connection = self.connection()
        cursor = connection.cursor()

        cursor.execute(command)
        result_row = cursor.fetchone()

        cursor.close()
        connection.close()

        if not result_row:
            #self.logger.warn("Not found Result {} in database".format(result_id))
            return None

        competitor = Competitor.from_dict({
            "id": result_row[1]
        })

        event_competition_id = result_row

        return [competitor, event_competition_id]
    
    def updateResult(self, result: Result, competitor: Competitor, event_competition_id: str ):
        connection = self.connection()
        cursor = connection.cursor()

        command = """
            UPDATE {results_table_name}
            SET result_type ='{competitor_id}', event_competition_id='{event_competition_id}', result='{result}'
            WHERE id='{result_id}'
        """.format(
            results_table_name=self.results_table_name,
            competitor_id=competitor.id,
            event_competition_id=event_competition_id,
            result=result.result,
            result_id=result.id
        )

        cursor.execute(command)

        connection.commit()
        cursor.close()
        connection.close()

    def readAllResultsForCompetitor(self, competitor_id):
        command = """
            SELECT {result_columns} FROM {results_table_name} WHERE competitor_id='{competitor_id}'
        """.format(
            result_columns=", ".join(self.result_columns),
            results_table_name=self.results_table_name,
            competitor_id = competitor_id
        )

        connection = self.connection()
        cursor = connection.cursor()

        cursor.execute(command)
        result_rows = cursor.fetchall()

        cursor.close()
        connection.close()

        if not result_rows:
            #self.logger.warn("Not found any results for competitor_id {} in database".format(competitor_id))
            return None
        
        results = []
        for result in result_rows:
            results.append(Result.from_dict({
                "id": result[0],
                "event_competition_id": result[2],
                "result": result[3]
            }))
        return results

db = ResultsDatabase()
db.create_structure()


