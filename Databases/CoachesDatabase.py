import logging
import mysql.connector
from Databases.settings import mysql_settings
from Entities.Coach import Coach
from typing import Iterable, Optional


class CoachesDatabase():
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.__coaches_table_name = "coaches"

        self.__logger = logging.getLogger("mysqlCoachesDatabase")
        self.__coach_columns = (
            "id",
            "name",
            "birth_date",
            "club_id",
            "competitors_number"
        )

    def connection(self):
        return mysql.connector.connect(
            user=mysql_settings["user"],
            password=mysql_settings["password"],
            host=mysql_settings["host"],
            database=mysql_settings["database"]
        )
    
    @property
    def logger(self):
        return self.__logger

    @property
    def coaches_table_name(self):
        return self.__coaches_table_name
    
    @property
    def coach_columns(self):
        return self.__coach_columns

    def create_structure(self) -> bool:
        connection = self.connection()
        cursor = connection.cursor()

        command = """CREATE TABLE IF NOT EXISTS {} (
            id VARCHAR(40) PRIMARY KEY,
            name VARCHAR(255),
            birth_date DATE,
            club_id VARCHAR(40),
            competitors_number INT
        )""".format(self.coaches_table_name)
        cursor.execute(command)

        cursor.close()
        connection.close()


    def exists(self, coach: Coach) -> bool:
        command = """
            SELECT EXISTS(SELECT * FROM {coaches_table_name} WHERE id="{club_id}" LIMIT 1)
        """.format(
            coaches_table_name=self.coaches_table_name,
            club_id=coach.id
        )
        connection = self.connection()
        cursor = connection.cursor()

        cursor.execute(command)
        result = cursor.fetchone()

        cursor.close()
        connection.close()

        if result[0]:
            #self.logger.debug("Coach {} exists in database".format(coach.id))
            return True
        else:
            return False
        

    def create_coach(self, coach: Coach) -> Optional[bool]:
        if self.exists(coach):
            return None

        command = """
            INSERT INTO {coaches_table_name} ({coach_columns})
            VALUES
            (
                '{id}',
                '{name}',
                STR_TO_DATE('{birth_date}', '%Y-%m-%d'),
                '{club_id}',
                '{competitors_number}'
            )
        """.format(
            coaches_table_name=self.coaches_table_name,
            coach_columns=", ".join(self.coach_columns),
            id=coach.id,
            name=coach.name,
            birth_date=coach.birth_date,
            club_id=coach.club_id,
            competitors_number=coach.competitors_number
        )

        connection = self.connection()
        cursor = connection.cursor()

        cursor.execute(command)
        connection.commit()
        cursor.close()
        connection.close()

        return True

    def readCoach(self, coach_id: str) -> Optional[Coach]:
        command = """
            SELECT {coach_columns} FROM {coaches_table_name} WHERE id='{coach_id}' LIMIT 1
        """.format(
            coach_columns=", ".join(self.coach_columns),
            coaches_table_name=self.coaches_table_name,
            coach_id=coach_id
        )

        connection = self.connection()
        cursor = connection.cursor()

        cursor.execute(command)
        coach_row = cursor.fetchone()

        cursor.close()
        connection.close()

        if not coach_row:
            #self.logger.warn("Not found coach {} in database".format(coach_id))
            return None

        coach = Coach.from_dict({
            "id": coach_row[0],
            "name": coach_row[1],
            "birth_date": coach_row[2].strftime("%Y-%m-%d"),
            "club_id": coach_row[3],
            "competitors_number": coach_row[3]

        })


        return coach
    
    def updateCoach(self, coach: Coach):
        connection = self.connection()
        cursor = connection.cursor()

        command = """
            UPDATE {coaches_table_name}
            SET name = '{name}', birth_date = STR_TO_DATE('{birth_date}', '%Y-%m-%d'), club_id = '{club_id}', competitors_number = '{competitors_number}'
            WHERE id = '{coach_id}'
        """.format(
            coaches_table_name=self.coaches_table_name,
            name=coach.name,
            birth_date=coach.birth_date,
            club_id=coach.club_id,
            coach_id=coach.id,
            competitors_number=coach.competitors_number
        )

        cursor.execute(command)

        connection.commit()
        cursor.close()
        connection.close()

    def removeCoach(self, coach_id):
        connection = self.connection()
        cursor = connection.cursor()

        command = """
            DELETE FROM {coaches_table_name}
            WHERE id = '{coach_id}'
        """.format(
            coaches_table_name=self.coaches_table_name,
            coach_id=coach_id,
        )

        cursor.execute(command)

        connection.commit()
        cursor.close()
        connection.close()


    def triggerDeleteCoach(self) -> Optional[Coach]:
        connection = self.connection()
        cursor = connection.cursor()

        command = """
            drop trigger if exists trigger_del_coach
            """
        cursor.execute(command)
        connection.commit()

        command = """

            create	trigger trigger_del_coach after delete
            on athletestatistics.coaches
            for each row
            update athletestatistics.competitors set coach_id=null where coach_id=old.id
         """

        cursor.execute(command)

        connection.commit()
        cursor.close()
        connection.close()

    def triggerAddCompetitorWithCoach(self) -> Optional[Coach]:
        connection = self.connection()
        cursor = connection.cursor()

        command = """
            drop trigger if exists trigger_add_coach
            """
        cursor.execute(command)
        connection.commit()

        command = """

            create	trigger trigger_add_coach after insert
            on athletestatistics.competitors
            for each row
            update athletestatistics.coaches set competitors_number=competitors_number+1 where id=new.coach_id
         """

        cursor.execute(command)

        connection.commit()
        cursor.close()
        connection.close()

    def triggerUpdateCompetitorWithCoach(self) -> Optional[Coach]:
        connection = self.connection()
        cursor = connection.cursor()

        command = """
            drop trigger if exists trigger_decrease_competitors_numbr
            """
        cursor.execute(command)
        connection.commit()

        command = """
            create	trigger trigger_decrease_competitors_numbr after update
            on athletestatistics.competitors
            for each row
            update athletestatistics.coaches set competitors_number=competitors_number-1 where id=old.coach_id
         """
        cursor.execute(command)

        command = """
            drop trigger if exists trigger_increase_competitors_numbr
            """
        cursor.execute(command)
        connection.commit()

        command = """
            create	trigger trigger_increase_competitors_numbr after update
            on athletestatistics.competitors
            for each row
            update athletestatistics.coaches set competitors_number=competitors_number+1 where id=new.coach_id
         """
        cursor.execute(command)

        connection.commit()

        cursor.close()
        connection.close()

    def readAllCoaches(self)-> Iterable[Coach]:
        command = """
            SELECT {coach_columns} FROM {coaches_table_name}
        """.format(
            coach_columns=", ".join(self.coach_columns),
            coaches_table_name=self.coaches_table_name
        )

        connection = self.connection()
        cursor = connection.cursor()
        
        cursor.execute(command)
        coaches_rows = cursor.fetchall()

        cursor.close()
        connection.close()

        coaches = []
        for coach in coaches_rows:
            coaches.append(Coach.from_dict({
                "id": coach[0],
                "name": coach[1],
                "birth_date": coach[2].strftime("%Y-%m-%d"),
                "club_id": coach[3],
                "competitors_number": coach[4]
            }))
        return coaches

    # def proposeCoachesProcedure(self):
    #     connection = self.connection()
    #     cursor = connection.cursor()

    #     command = """
    #         drop procedure if exists procedure_propose_coaches
    #         """
    #     cursor.execute(command)

    #     command = """
    #         create procedure procedure_propose_coaches()
    #         BEGIN
    #         SELECT id, competitors_number, name FROM athletestatistics.coaches WHERE competitors_number=6

    #         """
    #     cursor.execute(command)

    #     command = """
    #         END&&

    #         """
    #     cursor.execute(command)

    #     connection.commit()

        # commands = ["""
        #     create	procedure procedure_propose_coaches(
        #         in clubId varchar(40)
        #     )

        #     BEGIN
        #     DECLARE bDone INT;

        #     DECLARE var1 varchar(40);    -- or approriate type
        #     DECLARE var2 INT;
        #     DECLARE var3 VARCHAR(50);

        #     DECLARE curs CURSOR FOR SELECT id, competitors_number, name FROM athletestatistics.coaches WHERE competitors_number=
        #     (select min(competitors_number) from athletestatistics.coaches where club_id=clubId );
        #     DECLARE CONTINUE HANDLER FOR NOT FOUND SET bDone = 1;

        #     DROP TEMPORARY TABLE IF EXISTS tblResults;
        #     CREATE TEMPORARY TABLE IF NOT EXISTS tblResults  (
        #         coach_id varchar(40),
        #         competitors_number int,
        #         coach_name varchar(50)
        #     );

        #     OPEN curs;

        #     SET bDone = 0;
        #     REPEAT
        #         FETCH curs INTO var1, var2, var3;
        #         -- IF whatever_filtering_desired
        #         -- here for whatever_transformation_may_be_desired
        #         INSERT INTO tblResults VALUES (var1, var2, var3);
        #     --  END IF;
        #     UNTIL bDone END REPEAT;

        #     CLOSE curs;
        #     SELECT * FROM tblResults;

        #     END$$

        #  """
        # ]


        # if connection.is_connected():
        #     db_Info = connection.get_server_info()
        #     print("Connected to MySQL Server version ", db_Info, '\n')
        #     cursor = connection.cursor()

        #     for command in commands:
        #         for result in cursor.execute(command):
        #             if result.with_rows:
        #                 print("Rows produced by statement '{}':".format(
        #                 result.statement))
        #                 print(result.fetchall())
        #             else:
        #                 print("Number of rows affected by statement '{}': {}".format(
        #                 result.statement, result.rowcount), '\n')
        #     record = cursor.fetchall()


        # cursor.execute(commands)

        # connection.commit()

        # cursor.close()
        # connection.close()

