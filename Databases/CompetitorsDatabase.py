import logging
import mysql.connector
from Databases.settings import mysql_settings
from Entities.Competitor import Competitor
from typing import Iterable, Optional


class CompetitorsDatabase():
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.__competitors_table_name = "competitors"

        self.__logger = logging.getLogger("mysqlCompetitorsDatabase")
        self.__competitor_columns = (
            "id",
            "name",
            "birth_date",
            "coach_id"
        )

    def connection(self):
        return mysql.connector.connect(
            user=mysql_settings["user"],
            password=mysql_settings["password"],
            host=mysql_settings["host"],
            database=mysql_settings["database"]
        )
    
    @property
    def logger(self):
        return self.__logger

    @property
    def competitors_table_name(self):
        return self.__competitors_table_name
    
    @property
    def competitor_columns(self):
        return self.__competitor_columns

    def create_structure(self) -> bool:
        connection = self.connection()
        cursor = connection.cursor()

        command = """CREATE TABLE IF NOT EXISTS {} (
            id VARCHAR(40) PRIMARY KEY,
            name VARCHAR(255), 
            birth_date DATE,
            coach_id VARCHAR(40)
        )""".format(self.competitors_table_name)
        cursor.execute(command)

        cursor.close()
        connection.close()


    def exists(self, competitor: Competitor) -> bool:
        command = """
            SELECT EXISTS(SELECT * FROM {competitors_table_name} WHERE id="{competitor_id}" LIMIT 1)
        """.format(
            competitors_table_name=self.competitors_table_name,
            competitor_id=competitor.id
        )
        connection = self.connection()
        cursor = connection.cursor()

        cursor.execute(command)
        result = cursor.fetchone()

        cursor.close()
        connection.close()

        if result[0]:
            #self.logger.debug("Competitor {} exists in database".format(competitor.id))
            return True
        else:
            return False
        

    def create_competitor(self, competitor: Competitor) -> Optional[bool]:
        if self.exists(competitor):
            return None

        command = """
            INSERT INTO {competitors_table_name} ({competitor_columns})
            VALUES
            (
                '{id}',
                '{name}',
                STR_TO_DATE('{birth_date}', '%Y-%m-%d'),
                '{coach_id}'
            )
        """.format(
            competitors_table_name=self.competitors_table_name,
            competitor_columns=", ".join(self.competitor_columns),
            id=competitor.id,
            name=competitor.name,
            birth_date=competitor.birth_date,
            coach_id=competitor.coach_id
        )

        connection = self.connection()
        cursor = connection.cursor()

        cursor.execute(command)
        connection.commit()
        cursor.close()
        connection.close()

        return True

    def readCompetitor(self, competitor_id: str) -> Optional[Competitor]:
        command = """
            SELECT {competitor_columns} FROM {competitors_table_name} WHERE id='{competitor_id}' LIMIT 1
        """.format(
            competitor_columns=", ".join(self.competitor_columns),
            competitors_table_name=self.competitors_table_name,
            competitor_id=competitor_id
        )

        connection = self.connection()
        cursor = connection.cursor()

        cursor.execute(command)
        competitor_row = cursor.fetchone()

        cursor.close()
        connection.close()

        if not competitor_row:
            #self.logger.warn("Not found competitor {} in database".format(competitor_id))
            return None

        competitor = Competitor.from_dict({
            "id": competitor_row[0],
            "name": competitor_row[1],
            "birth_date": competitor_row[2].strftime("%Y-%m-%d"),
            "coach_id": competitor_row[3]

        })

        
        return competitor

    def updateCompetitor(self, id, competitor: Competitor):
        connection = self.connection()
        cursor = connection.cursor()

        command = """
            UPDATE {competitors_table_name}
            SET name = '{name}', birth_date = STR_TO_DATE('{birth_date}', '%Y-%m-%d'), coach_id = '{coach_id}'
            WHERE id = '{competitor_id}'
        """.format(
            competitors_table_name=self.competitors_table_name,
            name=competitor.name,
            birth_date=competitor.birth_date,
            coach_id=competitor.coach_id,
            competitor_id=id
        )

        cursor.execute(command)

        connection.commit()
        cursor.close()
        connection.close()

    def removeCompetitor(self, competitor_id):
        connection = self.connection()
        cursor = connection.cursor()

        command = """
            DELETE FROM {competitors_table_name}
            WHERE id = '{competitor_id}'
        """.format(
            competitors_table_name=self.competitors_table_name,
            competitor_id=competitor_id
        )

        cursor.execute(command)

        connection.commit()
        cursor.close()
        connection.close()
    
    def readAllCompetitors(self) -> Iterable[Competitor]:
        command = """
        SELECT {competitor_columns} FROM {competitors_table_name}
    """.format(
        competitor_columns=", ".join(self.competitor_columns),
        competitors_table_name=self.competitors_table_name,
    )

        connection = self.connection()
        cursor = connection.cursor()
        cursor.execute(command)
        competitor_rows = cursor.fetchall()

        cursor.close()
        connection.close()

        competitors = []
        for competitor in competitor_rows:
            competitors.append(Competitor.from_dict({
                "id": competitor[0],
                "name": competitor[1],
                "birth_date": competitor[2].strftime("%Y-%m-%d"),
                "coach_id": competitor[3]
            }))

        return competitors