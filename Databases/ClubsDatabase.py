import mysql.connector
from Databases.settings import mysql_settings
from Entities.Club import Club
from typing import Iterable, Optional

class ClubsDatabase():
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.__clubs_table_name = "clubs"
        self.__club_columns = (
            "id",
            "name",
            "city"
        )

    def connection(self):
        return mysql.connector.connect(
            user=mysql_settings["user"],
            password=mysql_settings["password"],
            host=mysql_settings["host"],
            database=mysql_settings["database"]
        )

    def create_structure(self) -> bool:
        connection = self.connection()
        cursor = connection.cursor()

        command = """CREATE TABLE IF NOT EXISTS {} (
            id VARCHAR(40) PRIMARY KEY,
            name VARCHAR(255),
            city VARCHAR(100)
        )""".format(self.clubs_table_name)
        cursor.execute(command)

        cursor.close()
        connection.close()

    # @property
    # def logger(self):
    #     return self.__logger

    @property
    def clubs_table_name(self):
        return self.__clubs_table_name
    
    @property
    def club_columns(self):
        return self.__club_columns


    def exists(self, club: Club) -> bool:
        command = """
            SELECT EXISTS(SELECT * FROM {clubs_table_name} WHERE id="{club_id}" LIMIT 1)
        """.format(
            clubs_table_name=self.clubs_table_name,
            club_id=club.id
        )
        connection = self.connection()
        cursor = connection.cursor()

        cursor.execute(command)
        result = cursor.fetchone()

        cursor.close()
        connection.close()

        if result[0]:
            # self.logger.debug("Club {} exists in database".format(club.id))
            return True
        else:
            return False
        

    def create_club(self, club: Club) -> Optional[bool]:
        if self.exists(club):
            return None

        command = """
            INSERT INTO {clubs_table_name} ({club_columns})
            VALUES
            (
                '{id}',
                '{name}',
                '{city}'
            )
        """.format(
            clubs_table_name=self.clubs_table_name,
            club_columns=", ".join(self.club_columns),
            id=club.id,
            name=club.name,
            city=club.city
        )

        connection = self.connection()
        cursor = connection.cursor()

        cursor.execute(command)
        connection.commit()
        cursor.close()
        connection.close()

        return True

    def readClub(self, club_id: str) -> Optional[Club]:
        command = """
            SELECT {club_columns} FROM {clubs_table_name} WHERE id='{club_id}' LIMIT 1
        """.format(
            club_columns=", ".join(self.club_columns),
            clubs_table_name=self.clubs_table_name,
            club_id=club_id
        )

        connection = self.connection()
        cursor = connection.cursor()

        cursor.execute(command)
        club_row = cursor.fetchone()

        cursor.close()
        connection.close()

        if not club_row:
            #self.logger.warn("Not found club {} in database".format(club_id))
            return None

        club = Club.from_dict({
            "id": club_row[0],
            "name": club_row[1],
            "city": club_row[2]

        })

        
        return club

    def updateClub(self, id, club: Club):
        connection = self.connection()
        cursor = connection.cursor()

        command = """
            UPDATE {clubs_table_name}
            SET name = '{name}', city = '{city}'
            WHERE id = '{club_id}'
        """.format(
            clubs_table_name=self.clubs_table_name,
            name=club.name,
            city=club.city,
            club_id=id
        )

        cursor.execute(command)

        connection.commit()
        cursor.close()
        connection.close()

    def readAllClubs(self) -> Iterable[Club]:
        command = """
            SELECT {club_columns} FROM {clubs_table_name} 
        """.format(
            club_columns=", ".join(self.club_columns),
            clubs_table_name=self.clubs_table_name,
        )

        connection = self.connection()
        cursor = connection.cursor()

        cursor.execute(command)
        club_rows = cursor.fetchall()

        cursor.close()
        connection.close()

        if not club_rows:
            #self.logger.warn("No clubs in database")
            return None
        else:
            clubs = []
            for club in club_rows: 
                clubs.append(Club.from_dict({
                "id": club[0],
                "name": club[1],
                "city": club[2]})
            )
            return clubs
        
    def removeClub(self, club_id):
        connection = self.connection()
        cursor = connection.cursor()

        command = """
            DELETE FROM {clubs_table_name}
            WHERE id = '{club_id}'
        """.format(
            clubs_table_name=self.clubs_table_name,
            club_id=club_id
        )
        
        cursor.execute(command)

        connection.commit()
        cursor.close()
        connection.close()