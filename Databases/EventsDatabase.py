import logging
import mysql.connector
from Databases.settings import mysql_settings
from Entities.Event import Event
from typing import Iterable, Optional


class EventsDatabase():
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.__events_table_name = "events"

        self.__logger = logging.getLogger("mysqlEventsDatabase")
        self.__event_columns = (
            "id",
            "name",
            "event_date",
            "place",
            "category"
        )

    def connection(self):
        return mysql.connector.connect(
            user=mysql_settings["user"],
            password=mysql_settings["password"],
            host=mysql_settings["host"],
            database=mysql_settings["database"]
        )
    
    @property
    def logger(self):
        return self.__logger

    @property
    def events_table_name(self):
        return self.__events_table_name
    
    @property
    def event_columns(self):
        return self.__event_columns

    def create_structure(self) -> bool:
        connection = self.connection()
        cursor = connection.cursor()

        command = """CREATE TABLE IF NOT EXISTS {} (
            id VARCHAR(40) PRIMARY KEY,
            name VARCHAR(255),
            event_date DATE,
            place VARCHAR(100),
            category VARCHAR(40)
        )""".format(self.events_table_name)
        cursor.execute(command)

        cursor.close()
        connection.close()


    def exists(self, event: Event) -> bool:
        command = """
            SELECT EXISTS(SELECT * FROM {events_table_name} WHERE id="{event_id}" LIMIT 1)
        """.format(
            events_table_name=self.events_table_name,
            event_id=event.id
        )
        connection = self.connection()
        cursor = connection.cursor()

        cursor.execute(command)
        result = cursor.fetchone()

        cursor.close()
        connection.close()

        if result[0]:
            #self.logger.debug("Event {} exists in database".format(event.id))
            return True
        else:
            return False
        

    def create_event(self, event: Event) -> Optional[bool]:
        if self.exists(event):
            return None

        command = """
            INSERT INTO {events_table_name} ({event_columns})
            VALUES
            (
                '{id}',
                '{name}',
                STR_TO_DATE('{event_date}', '%Y-%m-%d'),
                '{place}',
                '{category}'
            )
        """.format(
            events_table_name=self.events_table_name,
            event_columns=", ".join(self.event_columns),
            id=event.id,
            name=event.name,
            event_date=event.event_date,
            place=event.place,
            category=event.category
        )

        connection = self.connection()
        cursor = connection.cursor()

        cursor.execute(command)
        connection.commit()
        cursor.close()
        connection.close()

        return True

    def readEvent(self, event_id: str) -> Optional[Event]:
        command = """
            SELECT {event_columns} FROM {events_table_name} WHERE id='{event_id}' LIMIT 1
        """.format(
            event_columns=", ".join(self.event_columns),
            events_table_name=self.events_table_name,
            event_id=event_id
        )

        connection = self.connection()
        cursor = connection.cursor()

        cursor.execute(command)
        event_row = cursor.fetchone()

        cursor.close()
        connection.close()

        if not event_row:
            #self.logger.warn("Not found event {} in database".format(event_id))
            return None

        event = Event.from_dict({
            "id": event_row[0],
            "name": event_row[1],
            "event_date": event_row[2].strftime("%Y-%m-%d"),
            "place": event_row[3],
            "category": event_row[4]

        })

        
        return event

    def readAllEvents(self) -> Optional[Event]:
        command = """
            SELECT * FROM {events_table_name}
        """.format(
            event_columns=", ".join(self.event_columns),
            events_table_name=self.events_table_name,
        )

        connection = self.connection()
        cursor = connection.cursor()

        cursor.execute(command)
        event_rows = cursor.fetchall()

        cursor.close()
        connection.close()

        if not event_rows:
            #self.logger.warn("Not found event {} in database")
            return None
        
        events = []
        for event in event_rows:
            events.append(Event.from_dict({
                "id": event[0],
                "name": event[1],
                "event_date": event[2].strftime("%Y-%m-%d"),
                "place": event[3],
                "category": event[4]
            }))
        
        return events

    def updateEvent(self, event: Event):
        connection = self.connection()
        cursor = connection.cursor()

        command = """
            UPDATE {events_table_name}
            SET name = '{name}', event_date = STR_TO_DATE('{event_date}', '%Y-%m-%d'), place = '{place}', category='{category}'
            WHERE id = '{event_id}'
        """.format(
            events_table_name=self.events_table_name,
            name=event.name,
            event_date=event.event_date,
            place=event.place,
            category=event.category,
            event_id=event.id
        )

        cursor.execute(command)

        connection.commit()
        cursor.close()
        connection.close()

    def removeEvent(self, event_id):
        connection = self.connection()
        cursor = connection.cursor()

        command = """
            DELETE FROM {events_table_name}
            WHERE id = '{event_id}'
        """.format(
            events_table_name=self.events_table_name,
            event_id=event_id
        )
        
        cursor.execute(command)

        connection.commit()
        cursor.close()
        connection.close()