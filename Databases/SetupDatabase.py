from Databases.CompetitorsDatabase import CompetitorsDatabase
from Databases.CoachesDatabase import CoachesDatabase
from Databases.ClubsDatabase import ClubsDatabase
from Databases.EventsDatabase import EventsDatabase
from Databases.CompetitionsDatabase import CompetitionsDatabase
from Databases.RelaysDatabase import RelaysDatabase
from Databases.EventsCompetitionsDatabase import EventsCompetitionsDatabase
from Databases.EventsRelaysDatabase import EventsRelaysDatabase
from Databases.RelaysResultsDatabase import RelaysResultsDatabase
from Databases.ResultsDatabase import ResultsDatabase


competitorDatabase = CompetitorsDatabase()
coachesDatabase = CoachesDatabase()
clubDatabase = ClubsDatabase()
eventsDatabase = EventsDatabase()
competitionsDatabase = CompetitionsDatabase()
relaysDatabase = RelaysDatabase()
eventsCompetitionsDatabase = EventsCompetitionsDatabase()
eventsRelaysDatabase = EventsRelaysDatabase()
relaysResultsDatabase = RelaysResultsDatabase()
resultsDatabase = ResultsDatabase()

competitorDatabase.create_structure()
coachesDatabase.create_structure()
clubDatabase.create_structure()
eventsDatabase.create_structure()
competitionsDatabase.create_structure()
relaysDatabase.create_structure()
eventsCompetitionsDatabase.create_structure()
eventsRelaysDatabase.create_structure()
relaysDatabase.create_structure()
resultsDatabase.create_structure()
