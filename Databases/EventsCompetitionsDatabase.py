import mysql.connector
from Databases.settings import mysql_settings
from Entities.Competition import Competition
from Entities.Event import Event
from typing import Iterable, Optional


class EventsCompetitionsDatabase():
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.__events_competitions_table_name = "events_competitions"
        self.__event_competition_columns = (
            "id",
            "event_id",
            "competition_id"
        )

    def connection(self):
        return mysql.connector.connect(
            user=mysql_settings["user"],
            password=mysql_settings["password"],
            host=mysql_settings["host"],
            database=mysql_settings["database"]
        )

    def create_structure(self) -> bool:
        connection = self.connection()
        cursor = connection.cursor()

        command = """CREATE TABLE IF NOT EXISTS {} (
            id VARCHAR(40) PRIMARY KEY,
            event_id VARCHAR(40),
            competition_id VARCHAR(40)
            )""".format(self.events_competitions_table_name)

        cursor.execute(command)

        cursor.close()
        connection.close()

    @property
    def logger(self):
        return self.__logger

    @property
    def events_competitions_table_name(self):
        return self.__events_competitions_table_name
    
    @property
    def event_competition_columns(self):
        return self.__event_competition_columns


    def exists(self, event_competition: str) -> bool:
        command = """
            SELECT EXISTS(SELECT * FROM {events_competitions_table_name} WHERE id="{event_competition}" LIMIT 1)
        """.format(
            events_competitions_table_name=self.events_competitions_table_name,
            event_competition=event_competition
        )
        connection = self.connection()
        cursor = connection.cursor()

        cursor.execute(command)
        result = cursor.fetchone()

        cursor.close()
        connection.close()

        if result[0]:
            #self.logger.debug("Event-Competition relation {} exists in database".format(event_competition))
            return True
        else:
            return False

    def existsEventCompetition(self, event_id, competition_id) -> bool:
        command = """
            SELECT * FROM {events_competitions_table_name} WHERE event_id="{event_id}" AND competition_id="{competition_id}"
        """.format(
            events_competitions_table_name=self.events_competitions_table_name,
            event_id = event_id,
            competition_id = competition_id
        )
        connection = self.connection()
        cursor = connection.cursor()

        cursor.execute(command)
        result = cursor.fetchone()

        cursor.close()
        connection.close()

        if result is not None and result[0]:
            return result[0]
        else:
            return False        

    def create_event_competition_relation(self, event_competition_id,  event: Event, competition: Competition) -> Optional[bool]:
        if self.existsEventCompetition(event.id, competition.id):
            return None
        
        command = """
            INSERT INTO {events_competitions_table_name} ({event_competition_columns})
            VALUES
            (
                '{id}',
                '{event_id}',
                '{competition_id}'
            )
        """.format(
            events_competitions_table_name=self.events_competitions_table_name,
            event_competition_columns=", ".join(self.event_competition_columns),
            id=event_competition_id,
            event_id=event.id,
            competition_id=competition.id
        )

        connection = self.connection()
        cursor = connection.cursor()

        cursor.execute(command)
        connection.commit()
        cursor.close()
        connection.close()

        return True

    def readEventCompetitionRelation(self, event_competition_id: str):
        command = """
            SELECT {event_competition_columns} FROM {events_competitions_table_name} WHERE id='{event_competition_id}' LIMIT 1
        """.format(
            event_competition_columns=", ".join(self.event_competition_columns),
            events_competitions_table_name=self.events_competitions_table_name,
            event_competition_id=event_competition_id
        )

        connection = self.connection()
        cursor = connection.cursor()

        cursor.execute(command)
        event_competition_row = cursor.fetchone()

        cursor.close()
        connection.close()

        if not event_competition_row:
            #self.logger.warn("Not found Event-Competition relation {} in database".format(event_competition_id))
            return None

        event = Event.from_dict({
            "id": event_competition_row[1]
        })

        competition = Competition.from_dict({
            "id": event_competition_row[2]
        })

        return [event, competition]
    
    def updateEventCompetitionRelation(self, competition: Competition, event: Event, relation_id: str ):
        connection = self.connection()
        cursor = connection.cursor()

        command = """
            UPDATE {events_competitions_table_name}
            SET event_id ='{event_id}', competition_id='{competition_id}'
            WHERE id='{relation_id}'
        """.format(
            results_table_name=self.results_table_name,
            event_id=event.id,
            competition_id=competition.id,
            relation_id=relation_id
        )

        cursor.execute(command)

        connection.commit()
        cursor.close()
        connection.close()


db = EventsCompetitionsDatabase()
db.create_structure()


