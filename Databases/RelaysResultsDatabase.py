import mysql.connector
from Databases.settings import mysql_settings
from Entities.Competitor import Competitor
from Entities.Result import Result
from typing import Iterable, Optional


class RelaysResultsDatabase():
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.__relay_results_table_name = "relays_results"
        self.__relay_result_columns = (
            "id",
            "competitor1_id",
            "competitor2_id",
            "competitor3_id",
            "competitor4_id",
            "event_relay_id",
            "result"
        )

    def connection(self):
        return mysql.connector.connect(
            user=mysql_settings["user"],
            password=mysql_settings["password"],
            host=mysql_settings["host"],
            database=mysql_settings["database"]
        )

    def create_structure(self) -> bool:
        connection = self.connection()
        cursor = connection.cursor()

        command = """CREATE TABLE IF NOT EXISTS {} (
            id VARCHAR(40) PRIMARY KEY,
            competitor1_id VARCHAR(40),
            competitor2_id VARCHAR(40),
            competitor3_id VARCHAR(40),
            competitor4_id VARCHAR(40),
            event_relay_id VARCHAR(40),
            result FLOAT
            )""".format(self.relay_results_table_name)

        cursor.execute(command)

        cursor.close()
        connection.close()

    @property
    def logger(self):
        return self.__logger

    @property
    def relay_results_table_name(self):
        return self.__relay_results_table_name
    
    @property
    def relay_result_columns(self):
        return self.__relay_result_columns


    def exists(self, relay_result_id: str) -> bool:
        command = """
            SELECT * FROM {relay_results_table_name} WHERE id="{relay_result_id}"
        """.format(
            relay_results_table_name=self.relay_results_table_name,
            relay_result_id=relay_result_id
        )
        connection = self.connection()
        cursor = connection.cursor()

        cursor.execute(command)
        result = cursor.fetchone()

        cursor.close()
        connection.close()

        if result is None:
            #self.logger.debug("Resultn {} exists in database".format(result))
            return False
        else:
            return True
        

    def create_result(self, relay_result_id: str, competitor1: Competitor,
                    competitor2: Competitor, competitor3: Competitor, competitor4: Competitor,
                    event_relay_id: str, result: float) -> Optional[bool]:
        if self.exists(relay_result_id):
            return None
        
        command = """
            INSERT INTO {relay_results_table_name} ({relay_result_columns})
            VALUES
            (
                '{id}',
                '{competitor1_id}',
                '{competitor2_id}',
                '{competitor3_id}',
                '{competitor4_id}',
                '{event_relay_id}',
                '{result}'
            )
        """.format(
            relay_results_table_name=self.relay_results_table_name,
            relay_result_columns=", ".join(self.relay_result_columns),
            id=relay_result_id,
            competitor1_id=competitor1.id,
            competitor2_id=competitor2.id,
            competitor3_id=competitor3.id,
            competitor4_id=competitor4.id,
            event_relay_id=event_relay_id,
            result=result
        )

        connection = self.connection()
        cursor = connection.cursor()

        cursor.execute(command)
        connection.commit()
        cursor.close()
        connection.close()

        return True

    def readResult(self, result_id: str):
        command = """
            SELECT {result_columns} FROM {results_table_name} WHERE id='{result_id}' LIMIT 1
        """.format(
            result_columns=", ".join(self.relay_result_columns),
            results_table_name=self.relay_results_table_name,
            result_id=result_id
        )

        connection = self.connection()
        cursor = connection.cursor()

        cursor.execute(command)
        result_row = cursor.fetchone()

        cursor.close()
        connection.close()

        if not result_row:
            #self.logger.warn("Not found Result {} in database".format(result_id))
            return None

        competitor1 = Competitor.from_dict({
            "id": result_row[1]
        })
        competitor2 = Competitor.from_dict({
            "id": result_row[2]
        })
        competitor3 = Competitor.from_dict({
            "id": result_row[3]
        })
        competitor4 = Competitor.from_dict({
            "id": result_row[4]
        })

        event_relay_id = result_row

        return [competitor1, competitor2, competitor3, competitor4, event_relay_id]

    def readAllRelayResultsForCompetitor(self, competitor_id):
        command = """
            SELECT {result_columns} FROM {results_table_name} WHERE competitor1_id="{competitor_id}" OR competitor2_id="{competitor_id}" OR competitor3_id="{competitor_id}" OR competitor4_id="{competitor_id}"
        """.format(
            result_columns=", ".join(self.relay_result_columns),
            results_table_name=self.relay_results_table_name,
            competitor_id = competitor_id
        )

        connection = self.connection()
        cursor = connection.cursor()

        cursor.execute(command)
        result_rows = cursor.fetchall()

        cursor.close()
        connection.close()

        if not result_rows:
            #self.logger.warn("Not found Result {} in database".format(result_id))
            return None
        
        results = []
        for result in result_rows:
            results.append(Result.from_dict({
                "id": result[0],
                "event_competition_id": result[5],
                "result": result[6]
            }))
        return results
    
    def updateRelayResultRelation(self, competitor1: Competitor, competitor2: Competitor,
                                competitor3: Competitor, competitor4: Competitor,
                                event_relay_id: str, result: Result, relation_id: str ):
        connection = self.connection()
        cursor = connection.cursor()

        command = """
            UPDATE {relay_results_table_name}
            SET competitor1_id ='{competitor1_id}',
            competitor2_id ='{competitor2_id}',
            competitor3_id ='{competitor3_id}',
            competitor4_id ='{competitor4_id}',
            event_relay_id='{event_relay_id}',
            result='{result}'
            WHERE id='{relation_id}'
        """.format(
            relay_results_table_name=self.relay_results_table_name,
            competitor1_id=competitor1.id,
            competitor2_id=competitor2.id,
            competitor3_id=competitor3.id,
            competitor4_id=competitor4.id,
            event_relay_id=event_relay_id,
            result=result.result,
            relation_id=relation_id
        )

        cursor.execute(command)

        connection.commit()
        cursor.close()
        connection.close()


db = RelaysResultsDatabase()
db.create_structure()


