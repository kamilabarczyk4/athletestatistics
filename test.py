import mysql.connector
import json
from faker import Faker
from hashlib import sha1
from datetime import date
from typing import Iterable, Optional
from Databases.settings import mysql_settings

from Databases.CompetitorsDatabase import CompetitorsDatabase
from Databases.CoachesDatabase import CoachesDatabase
from Databases.ClubsDatabase import ClubsDatabase
from Databases.EventsDatabase import EventsDatabase
from Databases.CompetitionsDatabase import CompetitionsDatabase
from Databases.RelaysDatabase import RelaysDatabase
from Databases.EventsCompetitionsDatabase import EventsCompetitionsDatabase
from Databases.ResultsDatabase import ResultsDatabase

from Entities.Competitor import Competitor
from Entities.Coach import Coach
from Entities.Club import Club
from Entities.Event import Event
from Entities.Competition import Competition
from Entities.Relay import Relay
from Entities.Result import Result



mydb = mysql.connector.connect(
    user=mysql_settings["user"],
    password=mysql_settings["password"],
    host=mysql_settings["host"],
    database=mysql_settings["database"]
)

f = Faker()
sql_competitors_uc = CompetitorsDatabase()
sql_coaches_uc = CoachesDatabase()
sql_clubs_uc = ClubsDatabase()
sql_events_uc = EventsDatabase()
sql_competitions_uc = CompetitionsDatabase()
sql_relays_uc = RelaysDatabase()
eventsCompetitionsDatabase_uc = EventsCompetitionsDatabase()
resultDatabase_uc = ResultsDatabase()


def generate_competitor(coach_id: str) -> Optional[Competitor]:
    sp = f.simple_profile()
    # http://zetcode.com/python/faker/
    competitor_data = {
        "id": "7b3d21d119611a189fc48cf8621dae34717d903b",#sha1(bytes(sp["name"], "utf8")).hexdigest(),
        "name": "Kamila Barczyk",
        "birth_date": date.fromisoformat('1998-08-08'
        ).strftime("%Y-%m-%d"),
        "coach_id": coach_id
    }

    competitor = Competitor.from_dict(competitor_data=competitor_data)
    sql_competitors_uc.create_competitor(competitor)

    return competitor

def generate_coach(club_id:str) -> Optional[Coach]: #club_id: str
    sp = f.simple_profile()
    # http://zetcode.com/python/faker/
    coach_data = {
        "id": sha1(bytes(sp["name"], "utf8")).hexdigest(),
        "name": "Adam Malysz",
        "birth_date": date.fromisoformat('2005-05-31'
        ).strftime("%Y-%m-%d"),
        "club_id": club_id,
        "competitors_number": 6
    }

    # there should be coach = Coach(name, nickname, email)
    coach = Coach.from_dict(coach_data=coach_data)
    # print(coach)

    sql_coaches_uc.create_coach(coach)

    return coach

def generate_club() -> Optional[Club]:
    sp = f.simple_profile()
    # http://zetcode.com/python/faker/
    club_data = {
        "id": sha1(bytes(sp["name"], "utf8")).hexdigest(),
        "name": "Real",
        "city" : "Madryt"
    }

    # there should be coach = Coach(name, nickname, email)
    club = Club.from_dict(club_data=club_data)
    # print(club)

    sql_clubs_uc.create_club(club)

    return club

def generate_event() -> Optional[Event]:
    sp = f.simple_profile()

    competitor_data = {
        "id": "2222222222222222",#sha1(bytes(sp["name"], "utf8")).hexdigest(),
        "name": "Akademickie Mistrzostwa Polski",
        "event_date": date.fromisoformat('2023-05-09'
        ).strftime("%Y-%m-%d"),
        "place": "Krakow",
        "category": "Mistrzostwa Polski"
    }

    event = Event.from_dict(event_data=competitor_data)
    # print(event)

    sql_events_uc.create_event(event)

    return event

def generate_competition() -> Optional[Club]:
    sp = f.simple_profile()
    # http://zetcode.com/python/faker/
    competition_data = {
        "id": sha1(bytes(sp["name"], "utf8")).hexdigest(),
        "competition_name": "100m",
        "is_poly_competition" : int(False)
    }

    # there should be coach = Coach(name, nickname, email)
    competition = Competition.from_dict(competition_data=competition_data)
    print(competition)

    sql_competitions_uc.create_competition(competition)

    return competition

def generate_relay() -> Optional[Club]:
    sp = f.simple_profile()
    relay_data = {
        "id": sha1(bytes(sp["name"], "utf8")).hexdigest(),
        "relay_type": "4x400m"
    }

    relay = Relay.from_dict(relay_data=relay_data)
    print(relay)

    sql_relays_uc.create_relay(relay)

    return relay

def generate_eventCompetitionRelation(event: Event, competition: Competition) ->str:
    sp = f.simple_profile()
    id = sha1(bytes(sp["name"], "utf8")).hexdigest()
    eventsCompetitionsDatabase_uc.create_event_competition_relation(id, event, competition)
    return id

def generate_result(competitor):
    sp = f.simple_profile()
    result_data = {
        "id": sha1(bytes(sp["name"], "utf8")).hexdigest(),
        "competitor_id": competitor.id,
        "event_competition_id": "22",
        "result": "100"
    }
    result = Result.from_dict(result_data)
    resultDatabase_uc.create_result(result, competitor)
    return result

def readCompetitor(competitor_id):
    return sql_competitors_uc.readCompetitor(competitor_id)

def readAllCompetitors():
    return sql_competitors_uc.readAllCompetitors()   

def readAllClubs():
    return sql_clubs_uc.readAllClubs()

def readCoach(coach_id: str):
    sql_coaches_uc.readCoach(coach_id)

def readClub(club_id: str):
    sql_clubs_uc.readClub(club_id)

def readEvent(event_id: str):
    return sql_events_uc.readEvent(event_id)
def readAllEvents():
    return sql_events_uc.readAllEvents()

def readAllResultsOfCompetitor(competitor_id: str):
    return resultDatabase_uc.readAllResultsForCompetitor(competitor_id)
def existsEvent(event_id, competition_id):
    return eventsCompetitionsDatabase_uc.existsEventCompetition(event_id,competition_id)

# events = readAllEvents()
# for item in events:
#     print(item.name)
# print(existsEvent("2222222222222222", "282ef5ba57af2298cc4eaaba3151cdda2f508ddb"))

#club = generate_club()
#coach = generate_coach(club.id)
#competitor = generate_competitor(coach.id)
#coach2 = generate_coach(club.id)
#result = generate_result(competitor)
# competitor = readCompetitor("7b3d21d119611a189fc48cf8621dae34717d903b")
# results = readAllResultsOfCompetitor(competitor.id)
# print(len(results))
# for result in results:
#     print(result.result)

# # competitor = generate_competitor(coach2.id)
# # competitor = generate_competitor(coach.id)
# # competitor_data = {
# #         "id": "82577223724f948db7ac8320eb58a5c8c323686b",
# #         "name": "Piotr Lisek",
# #         "birth_date": date.fromisoformat('1998-08-08'
# #         ).strftime("%Y-%m-%d"),
# #         "coach_id": "75920de61f27323f0fb34977039e75703fc2c949"
# #     }

# # competitor = Competitor.from_dict(competitor_data=competitor_data)

# # sql_competitors_uc.updateCompetitorName(competitor)


# # sql_competitors_uc.updateCompetitorName(competitor)
#event = generate_event()
# #readCoach(coach.id)

# # mycursor = mydb.cursor()

# #readCompetitor(competitor.id)
# # competitory = readAllCompetitors()
# # for item in competitory:
# #     print(item.coach_id)
# #     print(readCoach(item.coach_id))


# #readClub(club.id)

# # sql_events_uc.readEvent(copmetition.id)

#competition = generate_competition()
# # sql_competitions_uc.readCompetition(competition.id)


#sql_coaches_uc.triggerDeleteCoach()
sql_coaches_uc.triggerAddCompetitorWithCoach()
#sql_coaches_uc.triggerUpdateCompetitorWithCoach()

#eventCompet_id = generate_eventCompetitionRelation(event, competition)
# #procedure test
# #result
# sp = f.simple_profile()
# result_data = {
#     "id": sha1(bytes(sp["name"], "utf8")).hexdigest(),
#     "result": 331.1111,
#     "event_competition_id": eventCompet_id
#     }

# result = Result.from_dict(result_data=result_data)
# def generateResutl():
#     sp = f.simple_profile()
#     # id = sha1(bytes(sp["name"], "utf8")).hexdigest()
#     resultDatabase_uc.create_result(result, competitor )

# generateResutl()

#event id 2222222222222222
# event = generate_event()


# # generateResutl()
# x = readAllClubs()
# for item in x:
#     print(readClub(item.id))
