set +e
REQUIREMENTS_PATH="requirements.txt"

echo -e "\n### Create virtual environment ###"
python -m venv .venv

echo -e "\n### Activate virtual environment ###"
source .venv/Scripts/activate

echo "Python path is $(which python)"

echo -e "\n### Upgrade pip ###"
python -m pip install --upgrade pip

echo -e "\n### Install dependencies ###"
python -m pip install -r $REQUIREMENTS_PATH

echo -e "\n### Install app ###"
python setup.py install