# from test import DB_Test
from PyQt5 import QtWidgets, QtCore
from PyQt5.QtWidgets import QApplication, QMainWindow, QTableWidgetItem
from PyQt5.QtCore import pyqtSignal, Qt
import sys
from Databases.CompetitorsDatabase import CompetitorsDatabase
from Databases.CoachesDatabase import CoachesDatabase
from Databases.ClubsDatabase import ClubsDatabase
from Databases.ResultsDatabase import ResultsDatabase
from Databases.EventsCompetitionsDatabase import EventsCompetitionsDatabase
from Databases.CompetitionsDatabase import CompetitionsDatabase
from Databases.EventsDatabase import EventsDatabase
from Databases.RelaysDatabase import RelaysDatabase
from Databases.EventsRelaysDatabase import EventsRelaysDatabase
from Databases.RelaysResultsDatabase import RelaysResultsDatabase
from Entities.Result import Result
from Entities.Competitor import Competitor
from Entities.Club import Club
from Entities.Coach import Coach
from Entities.Competition import Competition
from Entities.Event import Event
from Entities.Relay import Relay
from functools import partial
from hashlib import sha1

competitorsDB = CompetitorsDatabase()
clubsDB = ClubsDatabase()
coachesDB = CoachesDatabase()
resultsDB = ResultsDatabase()
eventsCompetitionsDB = EventsCompetitionsDatabase()
competitionsDB = CompetitionsDatabase()
eventsDB = EventsDatabase()
relaysDB = RelaysDatabase()
eventsRelaysDB = EventsRelaysDatabase()
relaysResultssDB = RelaysResultsDatabase()


class AddResultWindow(QMainWindow):
    result_added_signal = pyqtSignal()
    def __init__(self, parent = None):
        super(AddResultWindow, self).__init__(parent)
        self.setGeometry(250, 250, 300, 700)
        self.setWindowTitle("Okno dodawania wyników")
        self.labelCompetitor = QtWidgets.QLabel()
        self.comboboxCompetitor = QtWidgets.QComboBox()
        self.labelCompetitor1 = QtWidgets.QLabel()
        self.comboboxCompetitor1 = QtWidgets.QComboBox()
        self.labelCompetitor2 = QtWidgets.QLabel()
        self.comboboxCompetitor2 = QtWidgets.QComboBox()
        self.labelCompetitor3 = QtWidgets.QLabel()
        self.comboboxCompetitor3 = QtWidgets.QComboBox()
        self.labelRelay = QtWidgets.QLabel()
        self.checkboxRelay = QtWidgets.QCheckBox()
        self.labelEvent = QtWidgets.QLabel()
        self.comboboxEvent = QtWidgets.QComboBox()
        self.labelCompetition = QtWidgets.QLabel()
        self.comboboxCompetition = QtWidgets.QComboBox()
        self.labelResult = QtWidgets.QLabel()
        self.lineeditResult = QtWidgets.QLineEdit()
        self.buttonOK= QtWidgets.QPushButton()
        self.buttonClose = QtWidgets.QPushButton()
        self.initializeUI()

    def initializeUI(self):
        widget = QtWidgets.QWidget()
        self.setCentralWidget(widget)
        grid = QtWidgets.QGridLayout(widget)

        self.buttonOK.setText("Dodaj")
        self.buttonClose.setText("Anuluj")
        self.buttonClose.clicked.connect(self.close)
        self.labelRelay.setText("Sztafeta?: ")
        self.labelCompetitor.setText("Zawodnik #1: ")
        self.labelCompetitor1.setText("Zawodnik #2: ")
        self.labelCompetitor2.setText("Zawodnik #3: ")
        self.labelCompetitor3.setText("Zawodnik #4: ")
        self.labelCompetition.setText("Konkurencja: ")
        self.labelEvent.setText("Zawody: ")
        self.labelResult.setText("Wynik: ")
        self.lineeditResult.setMinimumWidth(150)
        self.labelCompetitor1.hide()
        self.labelCompetitor2.hide()
        self.labelCompetitor3.hide()
        self.comboboxCompetitor1.hide()
        self.comboboxCompetitor2.hide()
        self.comboboxCompetitor3.hide()
        self.competitionIDs = []
        self.relaysIDs = []
        def checkbox_state_changed():
            state = self.checkboxRelay.isChecked()
            if state == False:
                self.comboboxCompetition.clear()
                self.labelCompetitor1.hide()
                self.labelCompetitor2.hide()
                self.labelCompetitor3.hide()
                self.comboboxCompetitor1.hide()
                self.comboboxCompetitor2.hide()
                self.comboboxCompetitor3.hide()
                competitionsIDs = []
                competitions = competitionsDB.readAllCompetitions()
                self.comboboxCompetition.setMinimumWidth(150)
                for index, competition in enumerate(competitions):
                    self.comboboxCompetition.insertItem(index, str(competition.competition_name))
                    competitionsIDs.append(competition.id)

            else:
                self.comboboxCompetition.clear()
                self.labelCompetitor1.show()
                self.labelCompetitor2.show()
                self.labelCompetitor3.show()
                self.comboboxCompetitor1.show()
                self.comboboxCompetitor2.show()
                self.comboboxCompetitor3.show()
                self.relaysIDs = []
                relays = relaysDB.readAllRelays()
                self.comboboxCompetition.setMinimumWidth(150)
                for index, relay in enumerate(relays):
                    self.comboboxCompetition.insertItem(index, str(relay.relay_type))
                    self.relaysIDs.append(relay.id)

        self.checkboxRelay.stateChanged.connect(checkbox_state_changed)
        competitionsIDs = []
        relaysIDs = []
        competitions = competitionsDB.readAllCompetitions()
        for index, competition in enumerate(competitions):
            self.comboboxCompetition.insertItem(index, str(competition.competition_name))
            competitionsIDs.append(competition.id)
        self.competitorsIDs = []
        competitors = competitorsDB.readAllCompetitors()
        self.comboboxCompetitor.setMinimumWidth(150)
        for index, competitor in enumerate(competitors):
            self.comboboxCompetitor.insertItem(index, str(competitor.name))
            self.comboboxCompetitor1.insertItem(index, str(competitor.name))
            self.comboboxCompetitor2.insertItem(index, str(competitor.name))
            self.comboboxCompetitor3.insertItem(index, str(competitor.name))
            self.competitorsIDs.append(competitor.id)

        self.eventIDs = []
        events = eventsDB.readAllEvents()
        self.comboboxEvent.setMinimumWidth(150)
        for index, event in enumerate(events):
            self.comboboxEvent.insertItem(index, str(event.name))
            self.eventIDs.append(event.id)

        def AddResulttoDB():
            if not self.checkboxRelay.isChecked():
                result = float(self.lineeditResult.text())
                competitor_id = self.competitorsIDs[self.comboboxCompetitor.currentIndex()]
                competition_id = competitionsIDs[self.comboboxCompetition.currentIndex()]
                event_id = self.eventIDs[self.comboboxEvent.currentIndex()]
                event = eventsDB.readEvent(event_id)
                competition = competitionsDB.readCompetition(competition_id)
                temp = eventsCompetitionsDB.existsEventCompetition(event_id, competition_id)
                if temp is False:
                    event_competition_id = sha1(bytes(event_id+competition_id, "utf8")).hexdigest()
                else:
                    event_competition_id = eventsCompetitionsDB.existsEventCompetition(event_id, competition_id)
                eventsCompetitionsDB.create_event_competition_relation(str(event_competition_id), event, competition)
                new_result = Result(result, str(event_competition_id))
                competitor = competitorsDB.readCompetitor(competitor_id)
                resultsDB.create_result(new_result, competitor)
                self.buttonClose.click()
                self.result_added_signal.emit()
            else:
                result = float(self.lineeditResult.text())
                competitor_id = self.competitorsIDs[self.comboboxCompetitor.currentIndex()]
                competitor_id1 = self.competitorsIDs[self.comboboxCompetitor1.currentIndex()]
                competitor_id2 = self.competitorsIDs[self.comboboxCompetitor2.currentIndex()]
                competitor_id3 = self.competitorsIDs[self.comboboxCompetitor3.currentIndex()]
                relay_id = self.relaysIDs[self.comboboxCompetition.currentIndex()]
                event_id = self.eventIDs[self.comboboxEvent.currentIndex()]
                event = eventsDB.readEvent(event_id)
                relay = relaysDB.readRelay(relay_id)
                temp = eventsRelaysDB.exists(event_id, relay_id)
                if temp is False:
                    event_relay_id = sha1(bytes(event_id+relay_id, "utf8")).hexdigest()
                else:
                    event_relay_id = eventsRelaysDB.exists(event_id, relay_id)
                eventsRelaysDB.create_event_relay_relation(event_relay_id, event, relay)
                new_result = Result(result, str(event_relay_id))
                competitor = competitorsDB.readCompetitor(competitor_id)
                competitor1 = competitorsDB.readCompetitor(competitor_id1)
                competitor2 = competitorsDB.readCompetitor(competitor_id2)
                competitor3 = competitorsDB.readCompetitor(competitor_id3)
                relaysResultssDB.create_result(new_result.id, competitor, competitor1, competitor2, competitor3, event_relay_id, new_result.result)
                self.buttonClose.click()
                self.result_added_signal.emit()
        self.buttonOK.clicked.connect(AddResulttoDB)
        grid.addWidget(self.labelRelay, 0, 0)
        grid.addWidget(self.checkboxRelay, 0, 1)
        grid.addWidget(self.labelCompetitor, 1, 0)
        grid.addWidget(self.comboboxCompetitor, 1, 1)
        grid.addWidget(self.labelCompetitor1, 2, 0)
        grid.addWidget(self.comboboxCompetitor1, 2, 1)
        grid.addWidget(self.labelCompetitor2, 3, 0)
        grid.addWidget(self.comboboxCompetitor2, 3, 1)
        grid.addWidget(self.labelCompetitor3, 4, 0)
        grid.addWidget(self.comboboxCompetitor3, 4, 1)
        grid.addWidget(self.labelCompetition, 5, 0)
        grid.addWidget(self.comboboxCompetition, 5, 1)
        grid.addWidget(self.labelEvent, 6, 0)
        grid.addWidget(self.comboboxEvent, 6, 1)
        grid.addWidget(self.labelResult, 7, 0)
        grid.addWidget(self.lineeditResult, 7, 1)
        grid.addWidget(self.buttonClose, 8, 0)
        grid.addWidget(self.buttonOK, 8, 1)

class AddEventWindow(QMainWindow):
    event_added_signal = pyqtSignal()
    def __init__(self, parent = None):
        super(AddEventWindow, self).__init__(parent)
        self.setGeometry(250, 250, 300, 500)
        self.setWindowTitle("Okno dodawania zawodów")
        self.labelName = QtWidgets.QLabel()
        self.lineeditName = QtWidgets.QLineEdit()
        self.labelDate = QtWidgets.QLabel()
        self.lineeditDate = QtWidgets.QLineEdit()
        self.labelPlace = QtWidgets.QLabel()
        self.lineeditPlace = QtWidgets.QLineEdit()
        self.labelCategory = QtWidgets.QLabel()
        self.lineeditCategory = QtWidgets.QLineEdit()
        self.buttonOK= QtWidgets.QPushButton()
        self.buttonClose = QtWidgets.QPushButton()
        self.initializeUI()

    def initializeUI(self):
        widget = QtWidgets.QWidget()
        self.setCentralWidget(widget)
        grid = QtWidgets.QGridLayout(widget)

        self.buttonOK.setText("Dodaj")
        self.buttonClose.setText("Anuluj")
        self.buttonClose.clicked.connect(self.close)

        self.labelName.setText("Nazwa zawodów: ")
        self.lineeditName.setMinimumWidth(150)

        self.labelDate.setText("Data zawodów: ")
        self.lineeditDate.setMinimumWidth(150)

        self.labelPlace.setText("Miejsce zawodów: ")
        self.lineeditPlace.setMinimumWidth(150)

        self.labelCategory.setText("Ranga zawodów: ")
        self.lineeditCategory.setMinimumWidth(150)

        def AddEventtoDB():
            name = self.lineeditName.text()
            date = self.lineeditDate.text()
            place = self.lineeditPlace.text()
            category = self.lineeditCategory.text()
            new_event = Event(name, date, place, category)
            eventsDB.create_event(new_event)
            self.buttonClose.click()
            self.event_added_signal.emit()
        self.buttonOK.clicked.connect(AddEventtoDB)

        grid.addWidget(self.labelName, 0, 0)
        grid.addWidget(self.lineeditName, 0, 1)
        grid.addWidget(self.labelPlace, 1, 0)
        grid.addWidget(self.lineeditPlace, 1, 1)
        grid.addWidget(self.labelDate, 2, 0)
        grid.addWidget(self.lineeditDate, 2, 1)
        grid.addWidget(self.labelCategory, 3, 0)
        grid.addWidget(self.lineeditCategory, 3, 1)
        grid.addWidget(self.buttonClose, 4, 0)
        grid.addWidget(self.buttonOK, 4, 1)

class AddCompetitionWindow(QMainWindow):
    competition_added_signal = pyqtSignal()
    def __init__(self, parent = None):
        super(AddCompetitionWindow, self).__init__(parent)
        self.setGeometry(250, 250, 300, 500)
        self.setWindowTitle("Okno dodawania konkurencji")
        self.labelCompetition = QtWidgets.QLabel()
        self.lineeditCompetition = QtWidgets.QLineEdit()
        self.labelPoly = QtWidgets.QLabel()
        self.checkboxPoly = QtWidgets.QCheckBox()
        self.labelRelay = QtWidgets.QLabel()
        self.checkboxRelay = QtWidgets.QCheckBox()
        self.buttonOK= QtWidgets.QPushButton()
        self.buttonClose = QtWidgets.QPushButton()
        self.initializeUI()

    def initializeUI(self):
        widget = QtWidgets.QWidget()
        self.setCentralWidget(widget)
        grid = QtWidgets.QGridLayout(widget)

        self.buttonOK.setText("Dodaj")
        self.buttonClose.setText("Anuluj")
        self.buttonClose.clicked.connect(self.close)
        self.labelCompetition.setText("Konkurencja: ")
        self.lineeditCompetition.setMinimumWidth(150)

        self.labelPoly.setText("Wielobój?: ")

        self.labelRelay.setText("Sztafeta?: ")
        
        def checkbox_state_changed():
            state = self.checkboxRelay.isChecked()
            if state == True:
                self.checkboxPoly.setEnabled(False)
                self.checkboxPoly.setChecked(False)
            else:
                self.checkboxPoly.setEnabled(True)

        self.checkboxRelay.stateChanged.connect(checkbox_state_changed)

        def AddCompetitiontoDB():
            if self.checkboxRelay.isChecked():
                relay_name = self.lineeditCompetition.text()
                new_relay = Relay(relay_name)
                self.competition_added_signal.emit()
                self.buttonClose.click()
                relaysDB.create_relay(new_relay)
            else:
                if self.checkboxPoly.isChecked():
                    poly = 1
                else:
                    poly = 0
                competition = self.lineeditCompetition.text()
                new_competition = Competition(competition, poly)
                competitionsDB.create_competition(new_competition)
                self.competition_added_signal.emit()
                self.buttonClose.click()

        self.buttonOK.clicked.connect(AddCompetitiontoDB)

        grid.addWidget(self.labelCompetition, 0, 0)
        grid.addWidget(self.lineeditCompetition, 0, 1)
        grid.addWidget(self.labelPoly, 1, 0)
        grid.addWidget(self.checkboxPoly, 1, 1)
        grid.addWidget(self.labelRelay, 2, 0)
        grid.addWidget(self.checkboxRelay, 2, 1)
        grid.addWidget(self.buttonClose, 4, 0)
        grid.addWidget(self.buttonOK, 4, 1)

class AddCompetitorWindow(QMainWindow):
    competitor_added_signal = pyqtSignal()
    def __init__(self, parent = None):
        super(AddCompetitorWindow, self).__init__(parent)
        self.setGeometry(250, 250, 300, 500)
        self.setWindowTitle("Okno dodawania zawodnika")
        self.labelName = QtWidgets.QLabel()
        self.lineeditName = QtWidgets.QLineEdit()
        self.labelSurname = QtWidgets.QLabel()
        self.lineeditSurname = QtWidgets.QLineEdit()
        self.labelBirtdate = QtWidgets.QLabel()
        self.lineeditBirthdate = QtWidgets.QLineEdit()
        self.labelCoach = QtWidgets.QLabel()
        self.comboboxCoach = QtWidgets.QComboBox()
        self.buttonOK= QtWidgets.QPushButton()
        self.buttonClose = QtWidgets.QPushButton()
        self.initializeUI()

    def initializeUI(self):
        widget = QtWidgets.QWidget()
        self.setCentralWidget(widget)
        grid = QtWidgets.QGridLayout(widget)

        self.buttonOK.setText("Dodaj")
        self.buttonClose.setText("Anuluj")
        self.buttonClose.clicked.connect(self.close)
        self.labelName.setText("Imię: ")
        self.lineeditName.setMinimumWidth(150)

        self.labelSurname.setText("Nazwisko: ")
        self.lineeditSurname.setMinimumWidth(150)

        self.labelBirtdate.setText("Data urodzenia: ")
        self.lineeditBirthdate.setMinimumWidth(150)

        coaches = coachesDB.readAllCoaches()
        self.labelCoach.setText("Trener: ")
        self.comboboxCoach.setMinimumWidth(150)
        self.coaches_IDs = []
        for index, coach in enumerate(coaches):
            self.comboboxCoach.insertItem(index, "{coach}".format(coach = coach.name))
            self.coaches_IDs.append(coach.id)
        
        def AddCompetitortoDB():
            name = self.lineeditName.text() + " " + self.lineeditSurname.text()
            birthdate = self.lineeditBirthdate.text()
            coach_id = self.coaches_IDs[self.comboboxCoach.currentIndex()]
            new_Competitor = Competitor(name, birthdate, coach_id)
            competitorsDB.create_competitor(new_Competitor)
            self.buttonClose.click()
            self.competitor_added_signal.emit()
        self.buttonOK.clicked.connect(AddCompetitortoDB)

        grid.addWidget(self.labelName, 0, 0)
        grid.addWidget(self.lineeditName, 0, 1)
        grid.addWidget(self.labelSurname, 1, 0)
        grid.addWidget(self.lineeditSurname, 1, 1)
        grid.addWidget(self.labelBirtdate, 2, 0)
        grid.addWidget(self.lineeditBirthdate, 2, 1)
        grid.addWidget(self.labelCoach, 3, 0)
        grid.addWidget(self.comboboxCoach, 3, 1)
        grid.addWidget(self.buttonClose, 4, 0)
        grid.addWidget(self.buttonOK, 4, 1)

class AddCoachWindow(QMainWindow):
    def __init__(self, parent = None):
        super(AddCoachWindow, self).__init__(parent)
        self.setGeometry(250, 250, 300, 500)
        self.setWindowTitle("Okno dodawania trenera")
        self.labelName = QtWidgets.QLabel()
        self.lineeditName = QtWidgets.QLineEdit()
        self.labelSurname = QtWidgets.QLabel()
        self.lineeditSurname = QtWidgets.QLineEdit()
        self.labelBirtdate = QtWidgets.QLabel()
        self.lineeditBirthdate = QtWidgets.QLineEdit()
        self.labelClub = QtWidgets.QLabel()
        self.comboboxClub = QtWidgets.QComboBox()
        self.buttonOK= QtWidgets.QPushButton()
        self.buttonClose = QtWidgets.QPushButton()
        self.clubsIDs = []
        self.initializeUI()

    def initializeUI(self):
        widget = QtWidgets.QWidget()
        self.setCentralWidget(widget)
        grid = QtWidgets.QGridLayout(widget)

        self.buttonOK.setText("Dodaj")
        self.buttonClose.setText("Anuluj")
        self.buttonClose.clicked.connect(self.close)
        self.labelName.setText("Imię: ")
        self.lineeditName.setMinimumWidth(150)

        self.labelSurname.setText("Nazwisko: ")
        self.lineeditSurname.setMinimumWidth(150)

        self.labelBirtdate.setText("Data urodzenia: ")
        self.lineeditBirthdate.setMinimumWidth(150)

        clubs = clubsDB.readAllClubs()
        self.labelClub.setText("Klub: ")
        self.comboboxClub.setMinimumWidth(150)
        for index, club in enumerate(clubs):
            self.comboboxClub.insertItem(index, "{club_name} {club_city}".format(club_name = club.name, club_city = club.city))
            self.clubsIDs.append(club.id)

        def AddCoachtoDB():
            name = self.lineeditName.text() + " " + self.lineeditSurname.text()
            birthdate = self.lineeditBirthdate.text()
            club_id = self.clubsIDs[self.comboboxClub.currentIndex()]
            new_coach = Coach(name, birthdate, club_id)
            coachesDB.create_coach(new_coach)
            self.buttonClose.click()
        self.buttonOK.clicked.connect(AddCoachtoDB)

        grid.addWidget(self.labelName, 0, 0)
        grid.addWidget(self.lineeditName, 0, 1)
        grid.addWidget(self.labelSurname, 1, 0)
        grid.addWidget(self.lineeditSurname, 1, 1)
        grid.addWidget(self.labelBirtdate, 2, 0)
        grid.addWidget(self.lineeditBirthdate, 2, 1)
        grid.addWidget(self.labelClub, 3, 0)
        grid.addWidget(self.comboboxClub, 3, 1)
        grid.addWidget(self.buttonClose, 5, 0)
        grid.addWidget(self.buttonOK, 5, 1)

class AddClubWindow(QMainWindow):
    def __init__(self, parent = None):
        super(AddClubWindow, self).__init__(parent)
        self.setGeometry(250, 250, 300, 500)
        self.setWindowTitle("Okno dodawania klubu")
        self.labelName = QtWidgets.QLabel()
        self.lineeditName = QtWidgets.QLineEdit()
        self.labelCity = QtWidgets.QLabel()
        self.lineeditCity = QtWidgets.QLineEdit()
        self.buttonOK= QtWidgets.QPushButton()
        self.buttonClose = QtWidgets.QPushButton()
        self.initializeUI()

    def initializeUI(self):
        widget = QtWidgets.QWidget()
        self.setCentralWidget(widget)
        grid = QtWidgets.QGridLayout(widget)

        self.buttonOK.setText("Dodaj")
        self.buttonClose.setText("Anuluj")
        self.buttonClose.clicked.connect(self.close)

        self.labelName.setText("Nazwa klubu: ")
        self.lineeditName.setMinimumWidth(150)

        self.labelCity.setText("Miasto: ")
        self.lineeditCity.setMinimumWidth(150)

        def AddClubtoDB():
            name = self.lineeditName.text()
            city = self.lineeditCity.text()
            new_club = Club(name, city)
            clubsDB.create_club(new_club)
            self.buttonClose.click()
        self.buttonOK.clicked.connect(AddClubtoDB)
        
        grid.addWidget(self.labelName, 0, 0)
        grid.addWidget(self.lineeditName, 0, 1)
        grid.addWidget(self.labelCity, 1, 0)
        grid.addWidget(self.lineeditCity, 1, 1)
        grid.addWidget(self.buttonClose, 5, 0)
        grid.addWidget(self.buttonOK, 5, 1)

class RemoveClubWindow(QMainWindow):
    club_removed_signal = pyqtSignal()
    def __init__(self, parent = None):
        super(RemoveClubWindow, self).__init__(parent)
        self.setGeometry(250, 250, 300, 500)
        self.setWindowTitle("Okno usuwania klubu")
        self.labelName = QtWidgets.QLabel()
        self.comboboxName = QtWidgets.QComboBox()
        self.buttonOK= QtWidgets.QPushButton()
        self.buttonClose = QtWidgets.QPushButton()
        self.initializeUI()

    def initializeUI(self):
        widget = QtWidgets.QWidget()
        self.setCentralWidget(widget)
        grid = QtWidgets.QGridLayout(widget)

        self.buttonOK.setText("Usuń")
        self.buttonClose.setText("Anuluj")
        self.buttonClose.clicked.connect(self.close)

        self.labelName.setText("Nazwa klubu: ")
        self.comboboxName.setMinimumWidth(150)
        clubs = clubsDB.readAllClubs()
        clubsIDs = []
        for index, club in enumerate(clubs):
            self.comboboxName.insertItem(index, "{club_name} {club_city}".format(club_name = club.name, club_city = club.city))
            clubsIDs.append(club.id)

        def RemoveClubfromDB():
            clubsDB.removeClub(clubsIDs[self.comboboxName.currentIndex()])
            self.club_removed_signal.emit()
            self.buttonClose.click()
        self.buttonOK.clicked.connect(RemoveClubfromDB)
        grid.addWidget(self.labelName, 0, 0)
        grid.addWidget(self.comboboxName, 0, 1)
        grid.addWidget(self.buttonClose, 5, 0)
        grid.addWidget(self.buttonOK, 5, 1)

class RemoveCoachWindow(QMainWindow):
    coach_removed_signal = pyqtSignal()
    def __init__(self, parent = None):
        super(RemoveCoachWindow, self).__init__(parent)
        self.setGeometry(250, 250, 300, 500)
        self.setWindowTitle("Okno usuwania trenera")
        self.labelName = QtWidgets.QLabel()
        self.comboboxName = QtWidgets.QComboBox()
        self.buttonOK= QtWidgets.QPushButton()
        self.buttonClose = QtWidgets.QPushButton()
        self.initializeUI()

    def initializeUI(self):
        widget = QtWidgets.QWidget()
        self.setCentralWidget(widget)
        grid = QtWidgets.QGridLayout(widget)

        self.buttonOK.setText("Usuń")
        self.buttonClose.setText("Anuluj")
        self.buttonClose.clicked.connect(self.close)

        self.labelName.setText("Imię i nazwisko: ")
        self.comboboxName.setMinimumWidth(150)
        coaches = coachesDB.readAllCoaches()
        coachesIDs = []
        for index, coach in enumerate(coaches):
            self.comboboxName.insertItem(index, "{name}".format(name = coach.name))
            coachesIDs.append(coach.id)

        def RemoveCoachfromDB():
            coachesDB.removeCoach(coachesIDs[self.comboboxName.currentIndex()])
            self.coach_removed_signal.emit()
            self.buttonClose.click()
        self.buttonOK.clicked.connect(RemoveCoachfromDB)
        grid.addWidget(self.labelName, 0, 0)
        grid.addWidget(self.comboboxName, 0, 1)
        grid.addWidget(self.buttonClose, 5, 0)
        grid.addWidget(self.buttonOK, 5, 1)

class RemoveCompetitorWindow(QMainWindow):
    competitor_removed_signal = pyqtSignal()
    def __init__(self, parent = None):
        super(RemoveCompetitorWindow, self).__init__(parent)
        self.setGeometry(250, 250, 300, 500)
        self.setWindowTitle("Okno usuwania zawodnika")
        self.labelName = QtWidgets.QLabel()
        self.comboboxName = QtWidgets.QComboBox()
        self.buttonOK= QtWidgets.QPushButton()
        self.buttonClose = QtWidgets.QPushButton()
        self.initializeUI()

    def initializeUI(self):
        widget = QtWidgets.QWidget()
        self.setCentralWidget(widget)
        grid = QtWidgets.QGridLayout(widget)

        self.buttonOK.setText("Usuń")
        self.buttonClose.setText("Anuluj")
        self.buttonClose.clicked.connect(self.close)

        self.labelName.setText("Imię i nazwisko: ")
        self.comboboxName.setMinimumWidth(150)
        competitors = competitorsDB.readAllCompetitors()
        competitorsIDs = []
        for index, competitor in enumerate(competitors):
            self.comboboxName.insertItem(index, "{name}".format(name = competitor.name))
            competitorsIDs.append(competitor.id)

        def RemoveCompetitorfromDB():
            competitorsDB.removeCompetitor(competitorsIDs[self.comboboxName.currentIndex()])
            self.competitor_removed_signal.emit()
            self.buttonClose.click()
        self.buttonOK.clicked.connect(RemoveCompetitorfromDB)
        grid.addWidget(self.labelName, 0, 0)
        grid.addWidget(self.comboboxName, 0, 1)
        grid.addWidget(self.buttonClose, 5, 0)
        grid.addWidget(self.buttonOK, 5, 1)

class RemoveEventWindow(QMainWindow):
    event_removed_signal = pyqtSignal()
    def __init__(self, parent = None):
        super(RemoveEventWindow, self).__init__(parent)
        self.setGeometry(250, 250, 300, 500)
        self.setWindowTitle("Okno usuwania zawodów")
        self.labelName = QtWidgets.QLabel()
        self.comboboxName = QtWidgets.QComboBox()
        self.buttonOK= QtWidgets.QPushButton()
        self.buttonClose = QtWidgets.QPushButton()
        self.initializeUI()

    def initializeUI(self):
        widget = QtWidgets.QWidget()
        self.setCentralWidget(widget)
        grid = QtWidgets.QGridLayout(widget)

        self.buttonOK.setText("Usuń")
        self.buttonClose.setText("Anuluj")
        self.buttonClose.clicked.connect(self.close)

        self.labelName.setText("Nazwa zawodów: ")
        self.comboboxName.setMinimumWidth(150)
        events = eventsDB.readAllEvents()
        eventsIDs = []
        for index, event in enumerate(events):
            self.comboboxName.insertItem(index, "{event_name} {event_city}".format(event_name = event.name, event_city = event.place))
            eventsIDs.append(event.id)

        def RemoveEventfromDB():
            eventsDB.removeEvent(eventsIDs[self.comboboxName.currentIndex()])
            self.event_removed_signal.emit()
            self.buttonClose.click()
        self.buttonOK.clicked.connect(RemoveEventfromDB)
        grid.addWidget(self.labelName, 0, 0)
        grid.addWidget(self.comboboxName, 0, 1)
        grid.addWidget(self.buttonClose, 5, 0)
        grid.addWidget(self.buttonOK, 5, 1)

class RemoveCompetitionWindow(QMainWindow):
    competition_removed_signal = pyqtSignal()
    def __init__(self, parent = None):
        super(RemoveCompetitionWindow, self).__init__(parent)
        self.setGeometry(250, 250, 300, 500)
        self.setWindowTitle("Okno usuwania konkurencji")
        self.labelName = QtWidgets.QLabel()
        self.comboboxName = QtWidgets.QComboBox()
        self.buttonOK= QtWidgets.QPushButton()
        self.buttonClose = QtWidgets.QPushButton()
        self.initializeUI()

    def initializeUI(self):
        widget = QtWidgets.QWidget()
        self.setCentralWidget(widget)
        grid = QtWidgets.QGridLayout(widget)

        self.buttonOK.setText("Usuń")
        self.buttonClose.setText("Anuluj")
        self.buttonClose.clicked.connect(self.close)

        self.labelName.setText("Nazwa konkurencji: ")
        self.comboboxName.setMinimumWidth(150)
        competitions = competitionsDB.readAllCompetitions()
        competitionsIDs = []
        for index, competition in enumerate(competitions):
            self.comboboxName.insertItem(index, "{competition_name}".format(competition_name = competition.competition_name))
            competitionsIDs.append(competition.id)

        def RemoveCompetitionfromDB():
            competitionsDB.removeCompetition(competitionsIDs[self.comboboxName.currentIndex()])
            self.competition_removed_signal.emit()
            self.buttonClose.click()
        self.buttonOK.clicked.connect(RemoveCompetitionfromDB)
        grid.addWidget(self.labelName, 0, 0)
        grid.addWidget(self.comboboxName, 0, 1)
        grid.addWidget(self.buttonClose, 5, 0)
        grid.addWidget(self.buttonOK, 5, 1)

class RemoveResultWindow(QMainWindow):
    result_removed_signal = pyqtSignal()
    def __init__(self, parent = None):
        super(RemoveResultWindow, self).__init__(parent)
        self.setGeometry(250, 250, 300, 500)
        self.setWindowTitle("Okno usuwania konkurencji")
        self.labelName = QtWidgets.QLabel()
        self.comboboxName = QtWidgets.QComboBox()
        self.buttonOK= QtWidgets.QPushButton()
        self.buttonClose = QtWidgets.QPushButton()
        self.initializeUI()

    def initializeUI(self):
        widget = QtWidgets.QWidget()
        self.setCentralWidget(widget)
        grid = QtWidgets.QGridLayout(widget)

        self.buttonOK.setText("Usuń")
        self.buttonClose.setText("Anuluj")
        self.buttonClose.clicked.connect(self.close)

        self.labelName.setText("Nazwa konkurencji: ")
        self.comboboxName.setMinimumWidth(150)
        competitions = competitionsDB.readAllCompetitions()
        competitionsIDs = []
        for index, competition in enumerate(competitions):
            self.comboboxName.insertItem(index, "{competition_name}".format(club_name = competitions.name))
            competitionsIDs.append(competition.id)

        def RemoveCompetitionfromDB():
            competitionsDB.removeCompetition(competitionsIDs[self.comboboxName.currentIndex()])
            self.competition_removed_signal.emit()
            self.buttonClose.click()
        self.buttonOK.clicked.connect(RemoveCompetitionfromDB)
        grid.addWidget(self.labelName, 0, 0)
        grid.addWidget(self.comboboxName, 0, 1)
        grid.addWidget(self.buttonClose, 5, 0)
        grid.addWidget(self.buttonOK, 5, 1)

class ModifyClubWindow(QMainWindow):
    club_modified = pyqtSignal()
    def __init__(self, id, parent = None):
        super(ModifyClubWindow, self).__init__(parent)
        self.setGeometry(250, 250, 300, 500)
        self.setWindowTitle("Okno modyfikowania klubu")
        self.labelName = QtWidgets.QLabel()
        self.lineeditName = QtWidgets.QLineEdit()
        self.labelCity = QtWidgets.QLabel()
        self.lineeditCity = QtWidgets.QLineEdit()
        self.buttonOK= QtWidgets.QPushButton()
        self.buttonClose = QtWidgets.QPushButton()
        self.club_id = id
        
        self.initializeUI()

    def initializeUI(self):
        widget = QtWidgets.QWidget()
        self.setCentralWidget(widget)
        grid = QtWidgets.QGridLayout(widget)

        self.buttonOK.setText("Modyfikuj")
        self.buttonClose.setText("Anuluj")
        self.buttonClose.clicked.connect(self.close)
        club = clubsDB.readClub(self.club_id)
        self.labelName.setText("Nazwa klubu: ")
        self.lineeditName.setMinimumWidth(150)
        self.lineeditName.setText(club.name)
        self.lineeditCity.setText(club.city)

        self.labelCity.setText("Miasto: ")
        self.lineeditCity.setMinimumWidth(150)

        def ModifyClubinDB():
            name = self.lineeditName.text()
            city = self.lineeditCity.text()
            new_club = Club(name, city)
            clubsDB.updateClub(self.club_id, new_club)
            self.buttonClose.click()
            self.club_modified.emit()
        self.buttonOK.clicked.connect(ModifyClubinDB)
        
        grid.addWidget(self.labelName, 0, 0)
        grid.addWidget(self.lineeditName, 0, 1)
        grid.addWidget(self.labelCity, 1, 0)
        grid.addWidget(self.lineeditCity, 1, 1)
        grid.addWidget(self.buttonClose, 5, 0)
        grid.addWidget(self.buttonOK, 5, 1)

class ModifyCompetitorWindow(QMainWindow):
    competitor_modified_signal = pyqtSignal()
    def __init__(self, id, parent = None):
        super(ModifyCompetitorWindow, self).__init__(parent)
        self.setGeometry(250, 250, 300, 500)
        self.setWindowTitle("Okno modyfikowania zawodnika")
        self.labelName = QtWidgets.QLabel()
        self.lineeditName = QtWidgets.QLineEdit()
        self.labelSurname = QtWidgets.QLabel()
        self.lineeditSurname = QtWidgets.QLineEdit()
        self.labelBirtdate = QtWidgets.QLabel()
        self.lineeditBirthdate = QtWidgets.QLineEdit()
        self.labelCoach = QtWidgets.QLabel()
        self.comboboxCoach = QtWidgets.QComboBox()
        self.competitor_id = id
        self.buttonOK= QtWidgets.QPushButton()
        self.buttonClose = QtWidgets.QPushButton()
        self.initializeUI()

    def initializeUI(self):
        widget = QtWidgets.QWidget()
        self.setCentralWidget(widget)
        grid = QtWidgets.QGridLayout(widget)

        competitor = competitorsDB.readCompetitor(self.competitor_id)
        self.buttonOK.setText("Modyfikuj")
        self.buttonClose.setText("Anuluj")
        self.buttonClose.clicked.connect(self.close)
        self.labelName.setText("Imię: ")
        self.lineeditName.setMinimumWidth(150)
        self.lineeditName.setText(competitor.name.split(" ")[0])

        self.labelSurname.setText("Nazwisko: ")
        self.lineeditSurname.setMinimumWidth(150)
        self.lineeditSurname.setText(competitor.name.split(" ")[1])

        self.labelBirtdate.setText("Data urodzenia: ")
        self.lineeditBirthdate.setMinimumWidth(150)
        self.lineeditBirthdate.setText(str(competitor.birth_date))

        coaches = coachesDB.readAllCoaches()
        self.labelCoach.setText("Trener: ")
        self.comboboxCoach.setMinimumWidth(150)
        self.coaches_IDs = []
        for index, coach in enumerate(coaches):
            self.comboboxCoach.insertItem(index, "{coach}".format(coach = coach.name))
            self.coaches_IDs.append(coach.id)
            if coach.id == competitor.coach_id:
                actual_coach = index
        if actual_coach:
            self.comboboxCoach.setCurrentIndex(actual_coach)
        
        def ModifyCompetitorinDB():
            name = self.lineeditName.text() + " " + self.lineeditSurname.text()
            birthdate = self.lineeditBirthdate.text()
            coach_id = self.coaches_IDs[self.comboboxCoach.currentIndex()]
            new_Competitor = Competitor(name, birthdate, coach_id)
            competitorsDB.updateCompetitor(self.competitor_id, new_Competitor)
            self.buttonClose.click()
            self.competitor_modified_signal.emit()
        self.buttonOK.clicked.connect(ModifyCompetitorinDB)

        grid.addWidget(self.labelName, 0, 0)
        grid.addWidget(self.lineeditName, 0, 1)
        grid.addWidget(self.labelSurname, 1, 0)
        grid.addWidget(self.lineeditSurname, 1, 1)
        grid.addWidget(self.labelBirtdate, 2, 0)
        grid.addWidget(self.lineeditBirthdate, 2, 1)
        grid.addWidget(self.labelCoach, 3, 0)
        grid.addWidget(self.comboboxCoach, 3, 1)
        grid.addWidget(self.buttonClose, 4, 0)
        grid.addWidget(self.buttonOK, 4, 1)

class AddCoachWindow(QMainWindow):
    def __init__(self, parent = None):
        super(AddCoachWindow, self).__init__(parent)
        self.setGeometry(250, 250, 300, 500)
        self.setWindowTitle("Okno dodawania trenera")
        self.labelName = QtWidgets.QLabel()
        self.lineeditName = QtWidgets.QLineEdit()
        self.labelSurname = QtWidgets.QLabel()
        self.lineeditSurname = QtWidgets.QLineEdit()
        self.labelBirtdate = QtWidgets.QLabel()
        self.lineeditBirthdate = QtWidgets.QLineEdit()
        self.labelClub = QtWidgets.QLabel()
        self.comboboxClub = QtWidgets.QComboBox()
        self.buttonOK= QtWidgets.QPushButton()
        self.buttonClose = QtWidgets.QPushButton()
        self.clubsIDs = []
        self.initializeUI()

    def initializeUI(self):
        widget = QtWidgets.QWidget()
        self.setCentralWidget(widget)
        grid = QtWidgets.QGridLayout(widget)

        self.buttonOK.setText("Dodaj")
        self.buttonClose.setText("Anuluj")
        self.buttonClose.clicked.connect(self.close)
        self.labelName.setText("Imię: ")
        self.lineeditName.setMinimumWidth(150)

        self.labelSurname.setText("Nazwisko: ")
        self.lineeditSurname.setMinimumWidth(150)

        self.labelBirtdate.setText("Data urodzenia: ")
        self.lineeditBirthdate.setMinimumWidth(150)

        clubs = clubsDB.readAllClubs()
        self.labelClub.setText("Klub: ")
        self.comboboxClub.setMinimumWidth(150)
        for index, club in enumerate(clubs):
            self.comboboxClub.insertItem(index, "{club_name} {club_city}".format(club_name = club.name, club_city = club.city))
            self.clubsIDs.append(club.id)

        def AddCoachtoDB():
            name = self.lineeditName.text() + " " + self.lineeditSurname.text()
            birthdate = self.lineeditBirthdate.text()
            club_id = self.clubsIDs[self.comboboxClub.currentIndex()]
            new_coach = Coach(name, birthdate, club_id)
            coachesDB.create_coach(new_coach)
            self.buttonClose.click()
        self.buttonOK.clicked.connect(AddCoachtoDB)

        grid.addWidget(self.labelName, 0, 0)
        grid.addWidget(self.lineeditName, 0, 1)
        grid.addWidget(self.labelSurname, 1, 0)
        grid.addWidget(self.lineeditSurname, 1, 1)
        grid.addWidget(self.labelBirtdate, 2, 0)
        grid.addWidget(self.lineeditBirthdate, 2, 1)
        grid.addWidget(self.labelClub, 3, 0)
        grid.addWidget(self.comboboxClub, 3, 1)
        grid.addWidget(self.buttonClose, 5, 0)
        grid.addWidget(self.buttonOK, 5, 1)

class Window(QMainWindow):

    tableUserColumnNames = ["Imię i nazwisko", "Data urodzenia", "Klub", "Trener"]
    tableRecordColumnNames = ["Konkurencja", "Wynik", "Nazwa zawodów", "Miejsce", "Data", "Ranga", "Wielobój", "Sztafeta"]

    #initialize objects that appear in GUI window
    def __init__(self):
        super(Window, self).__init__()
        self.setGeometry(250, 250, 1800, 800)
        self.setWindowTitle("athleteStatistics")
        self.label = QtWidgets.QLabel()
        self.tableUser = QtWidgets.QTableWidget()
        self.tableRecord = QtWidgets.QTableWidget()
        self.buttonAdd = QtWidgets.QPushButton()
        self.comboboxAdd = QtWidgets.QComboBox()
        self.buttonRemove = QtWidgets.QPushButton()
        self.comboboxRemove = QtWidgets.QComboBox()
        self.buttonSort = QtWidgets.QPushButton()
        self.comboboxSortBy = QtWidgets.QComboBox()
        self.comboboxSortOrder = QtWidgets.QComboBox()
        self.buttonModify = QtWidgets.QPushButton()
        self.buttonExit = QtWidgets.QPushButton()
        self.selected_id =""
        self.selected_column =""
        self.initializeUI()
   
    def buttonAddClickAction(self):
        item = self.comboboxAdd.currentText()
        if item == "Zawodnik":
            dialog = AddCompetitorWindow(self)
            dialog.competitor_added_signal.connect(partial(self.updateUserTable, competitorsDB))
        elif item == "Trener":
            dialog = AddCoachWindow(self)
        elif item == "Klub":
            dialog = AddClubWindow(self)
        elif item == "Konkurencja":
            dialog = AddCompetitionWindow(self)
        elif item == "Zawody":
            dialog = AddEventWindow(self)
        elif item == "Wynik":
            dialog = AddResultWindow(self)
        dialog.show()
    
    def buttonRemoveClickAction(self):
        item = self.comboboxRemove.currentText()
        if item == "Zawodnik":
            dialog = RemoveCompetitorWindow(self)
            dialog.competitor_removed_signal.connect(partial(self.updateUserTable, competitorsDB))
        elif item == "Trener":
            dialog = RemoveCoachWindow(self)
            dialog.coach_removed_signal.connect(partial(self.updateUserTable, competitorsDB))
        elif item == "Klub":
            dialog = RemoveClubWindow(self)
            dialog.club_removed_signal.connect(partial(self.updateUserTable, competitorsDB))
        elif item == "Konkurencja":
            dialog = RemoveCompetitionWindow(self)
            dialog.competition_removed_signal.connect(partial(self.updateUserTable, competitorsDB))
        elif item == "Zawody":
            dialog = RemoveEventWindow(self)
            dialog.event_removed_signal.connect(partial(self.updateUserTable, competitorsDB))
        dialog.show()
    
    def sortButtonClickAction(self):
        if self.comboboxSortBy.currentText() in self.tableUserColumnNames:
            column_index = self.tableUserColumnNames.index(self.comboboxSortBy.currentText())
            if self.comboboxSortOrder.currentText() == "Rosnąco":
                self.tableUser.sortByColumn(column_index, QtCore.Qt.SortOrder.DescendingOrder)
            elif self.comboboxSortOrder.currentText() == "Malejąco":
                self.tableUser.sortByColumn(column_index, QtCore.Qt.SortOrder.AscendingOrder)
        else:
            column_index = self.tableRecordColumnNames.index(self.comboboxSortBy.currentText())
            if self.comboboxSortOrder.currentText() == "Rosnąco":
                self.tableRecord.sortByColumn(column_index, QtCore.Qt.SortOrder.DescendingOrder)
            elif self.comboboxSortOrder.currentText() == "Malejąco":
                self.tableRecord.sortByColumn(column_index, QtCore.Qt.SortOrder.AscendingOrder)

    def competitorTableClickAction(self):
        current_row = self.tableUser.currentRow()
        competitor_id = self.tableUser.item(current_row, 4).text()
        self.updateResultsTable(competitor_id)
    
    def recordTableClickAction(self):
        current_row = self.tableRecord.currentRow()
        current_column = self.tableRecord.currentColumn()
        item = self.tableRecord.item(current_row, current_column).data(1001)
        self.selected_id = item
        self.selected_column = self.tableRecord.horizontalHeaderItem(current_column).text()

    def userTableClickAction(self):
        current_row = self.tableUser.currentRow()
        current_column = self.tableUser.currentColumn()
        item = self.tableUser.item(current_row, current_column).data(1001)
        self.selected_id = item
        self.selected_column = self.tableUser.horizontalHeaderItem(current_column).text()
    
    def modifyButtonClickAction(self):
        if self.selected_column in ["Klub"]:
            self.dialog = ModifyClubWindow(self.selected_id)
            self.dialog.club_modified.connect(partial(self.updateUserTable, competitorsDB))
            self.dialog.show()
        elif self.selected_column in ["Imię i nazwisko", "Data urodzenia"]:
            self.dialog = ModifyCompetitorWindow(self.selected_id)
            self.dialog.competitor_modified_signal.connect(partial(self.updateUserTable, competitorsDB))
            self.dialog.show()
        elif self.selected_column in ["Trener"]:
            self.dialog = ModifyCoachWindow(self.selected_id)
            self.dialog.coach_modified_signal.connect(partial(self.updateUserTable, competitorsDB))
            self.dialog.show()

    #Initialize GUI - set sizes, positions, texts
    def initializeUI(self):
        
        widget = QtWidgets.QWidget()
        self.setCentralWidget(widget)

        self.buttonAdd.setText("Dodaj")
        self.buttonAdd.setMinimumWidth(150)
        self.buttonAdd.clicked.connect(self.buttonAddClickAction)

        self.comboboxAdd.setMinimumWidth(150)
        for index, item in enumerate(["Zawodnik", "Klub", "Trener", "Konkurencja", "Zawody", "Wynik"]):
            self.comboboxAdd.insertItem(index, item)

        self.buttonRemove.setText("Usuń")
        self.buttonRemove.setMinimumWidth(150)
        self.buttonRemove.clicked.connect(self.buttonRemoveClickAction)
        self.comboboxRemove.setMinimumWidth(150)
        for index, item in enumerate(["Zawodnik", "Klub", "Trener", "Konkurencja", "Zawody"]):
            self.comboboxRemove.insertItem(index, item)
 
        self.comboboxSortBy.setMinimumWidth(150)
        for index, item in enumerate(self.tableUserColumnNames + self.tableRecordColumnNames):
            self.comboboxSortBy.insertItem(index, item)

        self.comboboxSortOrder.setMinimumWidth(150)
        self.comboboxSortOrder.setMaximumWidth(150)
        self.comboboxSortOrder.insertItem(0, "Malejąco")
        self.comboboxSortOrder.insertItem(1, "Rosnąco")

        self.buttonSort.setText("Sortuj")
        self.buttonSort.setMinimumWidth(150)
        self.buttonSort.clicked.connect(self.sortButtonClickAction)

        self.buttonModify.setText("Modyfikuj")
        self.buttonModify.setMinimumWidth(150)
        self.buttonModify.clicked.connect(self.modifyButtonClickAction)

        self.buttonExit.setText("Wyjdź")
        self.buttonExit.setMinimumWidth(150)
        self.buttonExit.clicked.connect(self.close)

        self.updateUserTable(competitorsDB)
        
        rows = self.tableRecord.rowCount()
        self.tableRecord.setColumnCount(len(self.tableRecordColumnNames))
        self.tableRecord.setHorizontalHeaderLabels(self.tableRecordColumnNames)
        self.tableRecord.setRowCount(rows + 1)
        self.tableRecord.cellPressed.connect(self.recordTableClickAction)
        self.tableUser.cellPressed.connect(self.userTableClickAction)
        self.gridPlacement(widget)


    def updateUserTable(self, competitorsDB:CompetitorsDatabase):
        while self.tableUser.rowCount() > 0:
            self.tableUser.removeRow(0)
        competitors = competitorsDB.readAllCompetitors()
        rows = self.tableUser.rowCount()
        self.tableUser.setColumnCount(len(self.tableUserColumnNames)+1)
        self.tableUser.setHorizontalHeaderLabels(self.tableUserColumnNames)
        self.tableUser.setRowCount(len(competitors))
        for index, competitor in enumerate(competitors):
            coach = coachesDB.readCoach(str(competitor.coach_id))
            club_name = ""
            club_city = ""
            coach_name = ""
            if(coach):
                coach_name = coach.name
                club = clubsDB.readClub(str(coach.club_id))
                if(club):
                    club_city = club.city
                    club_name = club.name
            self.tableUser.setItem(index, 0, QTableWidgetItem(competitor.name))
            item = self.tableUser.item(index, 0)
            if item:
                if competitor:
                    item.setData(1001, competitor.id)
            self.tableUser.setItem(index, 1, QTableWidgetItem(str(competitor.birth_date)))
            item = self.tableUser.item(index, 1)
            if item:
                if competitor:
                    item.setData(1001, competitor.id)
            self.tableUser.setItem(index, 2, QTableWidgetItem(str("{club} {city}".format(club = club_name, city = club_city))))
            item = self.tableUser.item(index, 2)
            if item:
                if club:
                    item.setData(1001, club.id)
            self.tableUser.setItem(index, 3, QTableWidgetItem(coach_name))
            item = self.tableUser.item(index, 3)
            if item:
                if coach:
                    item.setData(1001, coach.id)
            self.tableUser.setItem(index, 4, QTableWidgetItem(str(competitor.id)))
        self.tableUser.setColumnHidden(4, True)
        self.tableUser.resizeColumnsToContents()
        self.tableUser.cellClicked.connect(self.competitorTableClickAction)

    def updateResultsTable(self, competitor_id):
        while self.tableRecord.rowCount() > 0:
            self.tableRecord.removeRow(0)
        row_counter = 0
        results = resultsDB.readAllResultsForCompetitor(competitor_id)
        if results:
            for index, item in enumerate(results):
                rowPosition = self.tableRecord.rowCount()
                self.tableRecord.insertRow(rowPosition)
                self.tableRecord.setItem(index, 1, QTableWidgetItem(str(item.result)))
                data = self.tableRecord.item(index, 1)
                if data:
                    if item:
                        data.setData(1001, item.id)
                eventsCompetitions_id = str(item.event_competition_id)
                eventsCompetitions = eventsCompetitionsDB.readEventCompetitionRelation(eventsCompetitions_id)
                event = eventsCompetitions[0]
                if event:
                    event_details = eventsDB.readEvent(event.id)
                    event_name = ""
                    event_place = ""
                    event_category = ""
                    event_date = ""
                    if event_details:
                        event_name = event_details.name
                        event_place = event_details.place
                        event_category = event_details.category
                        event_date = event_details.event_date
                competition = eventsCompetitions[1]
                if competition:
                    competition_details = competitionsDB.readCompetition(competition.id)
                    competition_name = ""
                    competition_is_poly = ""
                    if competition_details:
                        competition_name = competition_details.competition_name
                        competition_is_poly = competition_details.is_poly_competition
                self.tableRecord.setItem(index, 0, QTableWidgetItem(str(competition_name)))
                item = self.tableRecord.item(index, 0)
                item.setData(1001, competition.id)
                self.tableRecord.setItem(index, 2, QTableWidgetItem(str(event_name)))
                item = self.tableRecord.item(index, 2)
                item.setData(1001, event.id)
                self.tableRecord.setItem(index, 3, QTableWidgetItem(str(event_place)))
                item = self.tableRecord.item(index, 3)
                item.setData(1001, event.id)
                self.tableRecord.setItem(index, 4, QTableWidgetItem(str(event_date)))
                item = self.tableRecord.item(index, 4)
                item.setData(1001, event.id)
                self.tableRecord.setItem(index, 5, QTableWidgetItem(str(event_category)))
                item = self.tableRecord.item(index, 5)
                item.setData(1001, event.id)
                self.tableRecord.setItem(index, 6, QTableWidgetItem(str(competition_is_poly)))
                item = self.tableRecord.item(index, 6)
                item.setData(1001, competition.id)
                self.tableRecord.setItem(index, 7, QTableWidgetItem("0"))
                                         
                self.tableRecord.resizeColumnsToContents()
                row_counter = row_counter + 1
        relay_results = relaysResultssDB.readAllRelayResultsForCompetitor(competitor_id)
        if relay_results:
            for index, item in enumerate(relay_results):
                rowPosition = self.tableRecord.rowCount()
                self.tableRecord.insertRow(rowPosition)
                self.tableRecord.setItem(row_counter, 1, QTableWidgetItem(str(item.result)))
                data = self.tableRecord.item(row_counter, 1)
                if data:
                    if item:
                        data.setData(1001, item.id)
                eventsRelays_id = str(item.event_competition_id)
                eventsRelays = eventsRelaysDB.readEventRelayRelation(eventsRelays_id)
                event = eventsRelays[0]
                if event:
                    event_details = eventsDB.readEvent(event.id)
                    event_name = ""
                    event_place = ""
                    event_category = ""
                    event_date = ""
                    if event_details:
                        event_name = event_details.name
                        event_place = event_details.place
                        event_category = event_details.category
                        event_date = event_details.event_date
                relay = eventsRelays[1]
                if relay:
                    competition_details = relaysDB.readRelay(relay.id)
                    if competition_details:
                        relay_name = competition_details.relay_type
                self.tableRecord.setItem(row_counter, 0, QTableWidgetItem(str(relay_name)))
                data = self.tableRecord.item(row_counter, 2)
                if data:
                    data.setData(1001, relay.id)
                self.tableRecord.setItem(row_counter, 2, QTableWidgetItem(str(event_name)))
                data = self.tableRecord.item(row_counter, 2)
                if data:
                    data.setData(1001, event.id)
                self.tableRecord.setItem(row_counter, 3, QTableWidgetItem(str(event_place)))
                data = self.tableRecord.item(row_counter, 3)
                if data:
                    data.setData(1001, event.id)
                self.tableRecord.setItem(row_counter, 4, QTableWidgetItem(str(event_date)))
                data = self.tableRecord.item(row_counter, 4)
                if data:
                    data.setData(1001, event.id)
                self.tableRecord.setItem(row_counter, 5, QTableWidgetItem(str(event_category)))
                data = self.tableRecord.item(row_counter, 5)
                if data:
                    data.setData(1001, event.id)
                self.tableRecord.setItem(row_counter, 6, QTableWidgetItem("0"))
                self.tableRecord.setItem(row_counter, 7, QTableWidgetItem("1"))
                row_counter = row_counter+1
                self.tableRecord.resizeColumnsToContents()
        

    def gridPlacement(self, widget:QtWidgets.QWidget):
        grid = QtWidgets.QGridLayout(widget)
        grid.addWidget(self.buttonAdd, 0, 0)
        grid.addWidget(self.comboboxAdd, 0, 1)
        grid.addWidget(self.buttonRemove, 1, 0)
        grid.addWidget(self.comboboxRemove, 1, 1)
        grid.addWidget(self.buttonSort, 3, 0)
        grid.addWidget(self.comboboxSortBy, 3, 1)
        grid.addWidget(self.comboboxSortOrder, 3, 2)
        grid.addWidget(self.buttonModify, 4, 0)
        grid.addWidget(self.buttonExit, 4, 5)
        grid.addWidget(self.tableUser, 5, 0, 1, 2) 
        grid.addWidget(self.tableRecord, 5, 2, 1, 4)


def window():
    
    app = QApplication(sys.argv)
    window = Window()
    window.show()
    sys.exit(app.exec_())

window()