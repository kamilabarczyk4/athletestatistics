from hashlib import sha1
import json


class Competition():
    def __init__(self, competition_name, is_poly_competition):
        print("init Competition {}".format(competition_name))
        self.__competition_name = competition_name
        self.__is_poly_competition = is_poly_competition

        self.__id = sha1(bytes(competition_name, "utf8")).hexdigest()

    def __str__(self):
        return json.dumps(self.to_dict())

    def __eq__(self, other):
        if not isinstance(other, Competition):
            raise Exception("Not Competition class instance")
        return self.to_dict() == other.to_dict()

    def to_dict(self):
        d = {}
        for key in self.__dict__:
            k = key.replace("_{}__".format(__class__.__name__), "")
            d[k] = self.__dict__[key]
        return d

    @classmethod
    def from_dict(self, competition_data) -> "Competition":
        competition = object.__new__(__class__)
        for key in competition_data:
            competition.__setattr__(
                "_{}__{}".format(__class__.__name__, key),
                competition_data[key]
            )
        return competition

    @property
    def id(self):
        return self.__id

    @property
    def competition_name(self):
        return self.__competition_name
    
    @property
    def is_poly_competition(self):
        return self.__is_poly_competition