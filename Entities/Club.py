from hashlib import sha1
import json


class Club():
    def __init__(self, name, city):
        print("init Club {}".format(name))
        self.__name = name
        self.__city = city

        self.__id = sha1(bytes(name, "utf8")).hexdigest()

    def __str__(self):
        return json.dumps(self.to_dict())

    def __eq__(self, other):
        if not isinstance(other, Club):
            raise Exception("Not Club class instance")
        return self.to_dict() == other.to_dict()

    def to_dict(self):
        d = {}
        for key in self.__dict__:
            k = key.replace("_{}__".format(__class__.__name__), "")
            d[k] = self.__dict__[key]
        return d

    @classmethod
    def from_dict(self, club_data) -> "Club":
        club = object.__new__(__class__)
        for key in club_data:
            club.__setattr__(
                "_{}__{}".format(__class__.__name__, key),
                club_data[key]
            )
        return club

    @property
    def id(self):
        return self.__id

    @property
    def name(self):
        return self.__name
    
    @property
    def city(self):
        return self.__city