from datetime import date, timezone
from hashlib import sha1
import json


class Competitor():
    def __init__(self, name, birthdate, coach_id):
        print("init Competitor {}".format(name))
        self.__name = name

        self.__id = sha1(bytes(name, "utf8")).hexdigest()
        self.__birth_date = birthdate
        self.__coach_id = coach_id

    def __str__(self):
        return json.dumps(self.to_dict())

    def __eq__(self, other):
        if not isinstance(other, Competitor):
            raise Exception("Not Competitor class instance")
        return self.to_dict() == other.to_dict()

    def to_dict(self):
        d = {}
        for key in self.__dict__:
            k = key.replace("_{}__".format(__class__.__name__), "")
            if (k == "birth_date"):
                d[k] = self.__dict__[key].strftime("%Y-%m-%d")
            else:
                d[k] = self.__dict__[key]
        return d

    @classmethod
    def from_dict(self, competitor_data) -> "Competitor":
        competitor = object.__new__(__class__)
        for key in competitor_data:
            if (key == "birth_date"):
                competitor.__setattr__(
                    "_{}__birth_date".format(__class__.__name__),
                    date.fromisoformat(competitor_data[key])
                )
            else:
                competitor.__setattr__(
                    "_{}__{}".format(__class__.__name__, key),
                    competitor_data[key]
                )
        return competitor

    @property
    def id(self):
        return self.__id

    @property
    def name(self):
        return self.__name
    
    @property
    def birth_date(self):
        return self.__birth_date

    @property
    def coach_id(self):
        return self.__coach_id