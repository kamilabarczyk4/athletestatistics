from datetime import date, timezone
from hashlib import sha1
import json


class Coach():
    def __init__(self, name, birthdate, club_id):
        print("init Coach {}".format(name))
        self.__name = name

        self.__id = sha1(bytes(name, "utf8")).hexdigest()
        self.__birth_date = birthdate
        self.__club_id = club_id
        self.__competitors_number = 0

    def __str__(self):
        return json.dumps(self.to_dict())

    def __eq__(self, other):
        if not isinstance(other, Coach):
            raise Exception("Not Coach class instance")
        return self.to_dict() == other.to_dict()

    def to_dict(self):
        d = {}
        for key in self.__dict__:
            k = key.replace("_{}__".format(__class__.__name__), "")
            if (k == "birth_date"):
                d[k] = self.__dict__[key].strftime("%Y-%m-%d")
            else:
                d[k] = self.__dict__[key]
        return d

    @classmethod
    def from_dict(self, coach_data) -> "Coach":
        coach = object.__new__(__class__)
        for key in coach_data:
            if (key == "birth_date"):
                coach.__setattr__(
                    "_{}__birth_date".format(__class__.__name__),
                    date.fromisoformat(coach_data[key])
                )
            else:
                coach.__setattr__(
                    "_{}__{}".format(__class__.__name__, key),
                    coach_data[key]
                )
        return coach

    @property
    def id(self):
        return self.__id

    @property
    def name(self):
        return self.__name
    
    @property
    def birth_date(self):
        return self.__birth_date

    @property
    def club_id(self):
        return self.__club_id
    
    @property
    def competitors_number(self):
        return self.__competitors_number