from datetime import date, timezone
from hashlib import sha1
import json


class Event():
    def __init__(self, name, date, place, category):
        print("init Event {}".format(name))
        self.__name = name

        self.__id = sha1(bytes(name, "utf8")).hexdigest()
        self.__place = place
        self.__category = category
        self.__event_date = date

    def __str__(self):
        return json.dumps(self.to_dict())

    def __eq__(self, other):
        if not isinstance(other, Event):
            raise Exception("Not Event class instance")
        return self.to_dict() == other.to_dict()

    def to_dict(self):
        d = {}
        for key in self.__dict__:
            k = key.replace("_{}__".format(__class__.__name__), "")
            if (k == "event_date"):
                d[k] = self.__dict__[key].strftime("%Y-%m-%d")
            else:
                d[k] = self.__dict__[key]
        return d

    @classmethod
    def from_dict(self, event_data) -> "Event":
        event = object.__new__(__class__)
        for key in event_data:
            if (key == "event_date"):
                event.__setattr__(
                    "_{}__event_date".format(__class__.__name__),
                    date.fromisoformat(event_data[key])
                )
            else:
                event.__setattr__(
                    "_{}__{}".format(__class__.__name__, key),
                    event_data[key]
                )
        return event

    @property
    def id(self):
        return self.__id

    @property
    def name(self):
        return self.__name
    
    @property
    def event_date(self):
        return self.__event_date

    @property
    def place(self):
        return self.__place
    
    @property
    def category(self):
        return self.__category