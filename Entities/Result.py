from hashlib import sha1
import json


class Result():
    def __init__(self, result, event_competition_id):
        print("init Result {}".format(result))
        self.__result = result
        self.__event_competition_id = event_competition_id

        self.__id = sha1(bytes(str(result), "utf8")).hexdigest()

    def __str__(self):
        return json.dumps(self.to_dict())

    def __eq__(self, other):
        if not isinstance(other, Result):
            raise Exception("Not Result class instance")
        return self.to_dict() == other.to_dict()

    def to_dict(self):
        d = {}
        for key in self.__dict__:
            k = key.replace("_{}__".format(__class__.__name__), "")
            d[k] = self.__dict__[key]
        return d

    @classmethod
    def from_dict(self, result_data) -> "Result":
        result = object.__new__(__class__)
        for key in result_data:
            result.__setattr__(
                "_{}__{}".format(__class__.__name__, key),
                result_data[key]
            )
        return result

    @property
    def id(self):
        return self.__id

    @property
    def result(self):
        return self.__result

    @property
    def event_competition_id(self):
        return self.__event_competition_id
