from hashlib import sha1
import json


class Relay():
    def __init__(self, relay_type, ):
        print("init Relay {}".format(relay_type))
        self.__relay_type = relay_type

        self.__id = sha1(bytes(relay_type, "utf8")).hexdigest()

    def __str__(self):
        return json.dumps(self.to_dict())

    def __eq__(self, other):
        if not isinstance(other, Relay):
            raise Exception("Not Relay class instance")
        return self.to_dict() == other.to_dict()

    def to_dict(self):
        d = {}
        for key in self.__dict__:
            k = key.replace("_{}__".format(__class__.__name__), "")
            d[k] = self.__dict__[key]
        return d

    @classmethod
    def from_dict(self, relay_data) -> "Relay":
        relay = object.__new__(__class__)
        for key in relay_data:
            relay.__setattr__(
                "_{}__{}".format(__class__.__name__, key),
                relay_data[key]
            )
        return relay

    @property
    def id(self):
        return self.__id

    @property
    def relay_type(self):
        return self.__relay_type
